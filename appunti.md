# Blue Comet

Documentazione del costruttore/venditore
----------------------------------------

* [link](https://eurotronic.org/produkte/bluetooth-heizkoerperthermostate/comet-blue/)
* [Datenblatt](https://eurotronic.org/wp-content/uploads/2018/08/CometBlue-WB-de.pdf)
* [Bedienungsanweisung](https://eurotronic.org/wp-content/uploads/2018/08/Comet-Blue-BAL-de-A4.pdf)


Caratteristiche/funzionalità di base
------------------------------------

* Funkstandard: Bluetooth 4.0
* Reichweite: 10 – 50 m
* Versorgungsspannung: 2 x 1,5V AA/Mignon/LR6
* Funkfrequenz: 2,4 GHz
* Gewindeanschluss: M30 x 1.5mm
* Abmessungen (BxHxT): 55 x 65 x 78 mm
* Gewicht (netto): 160g (inkl. Batterien)
* Schutzart: IP20
* Verschmutzungsgrad: 2
* Anzeigefeld: Digital
* Farbe: Weiß
* Schaltzeiten pro Tag: 8
* Schaltzeiten pro Woche: 7
* Regelbereich: 8° C bis 28° C
* Ansprechempfindlichkeit: 0,1° C
* Selbsttest: 1 x wöchentlich

Rispetto alla funzione "Finestra aperta", dicono
* Wenn der Raum gelüftet wird, erkennt Comet Blue dies direkt. Bei einer Veränderung der Temperatur um -1,5 Grad Celsius innerhalb von 2,5 Minuten wird das Ventil vom Comet Blue vollautomatisch geschlossen. Erst bei einem Temperaturanstieg bzw. nach einer individuell einstellbaren Zeit wird das Ventil wieder geöffnet. 

Prodotti equivalenti:
--------------------
* [Comet Blue](https://eurotronic.org/produkte/bluetooth-heizkoerperthermostate/comet-blue/) (by [Eurotronic](https://eurotronic.org/)). Rispetto a Xavax pare che abbia in più la possibilità di usare un "Progmatic-Programmierstick"
* [Xavax](https://www.xavax.eu/00111971/xavax-funk-heizkoerperregler-bluetooth-) (by [Hama](https://www.xavax.eu/)) :  sembra identico al Comet Blue di Eurotronic. Secondo una recensione Amazon pare sia possibile pure il Progmatic-Stick, però il manuale istruzioni non ne parla e gli manca l'ingresso USB...
* [Sygonix HT100 BT](https://www.conrad.de/de/p/sygonix-ht100-bt-funk-heizkoerperthermostat-elektronisch-8-bis-28-c-1497888.html) (by [Conrad](https://www.conrad.de/)) : quasi identico al Comet Blue / Xavax, ma con una lucina Led (che potrebbe dar fastidio di notte)

Interessante notare che Xavax risulta avere EUROtronic Gmbh come Manufaturer e come soft/firmware COBL0126 ovvero 0.0.6-sygonix1.
Questo potrebbe far pensare che alcune funzioni sul Xavax non hanno effetti in quanto gli manca il Led e non ha la porticina USB per il "Progmatic-Programmierstick"

A giudicare dai progetti in internet, questi prodotti sono in commercio almeno da gennaio 2016. 
Heise.de dice che elenca il Comet Blue fin dall'agosto 2015 (test pubblicato in c't 15/2015).    
Il flyer della EUROtronic indica luglio 2014. 
La dichiarazione di conformità del Xavax risulta essere rilasciata nel marzo 2015
e quella del Sygonix di novembre 2015.

Altro
-----
### Come sapere se è attivata una "assenza"

Quando un'assenza (Holiday) è in vigore, allora il Byte delle ore di inizio assenza viene impostato dal device su 128 (hex=80, 0x80, 0x0080, 10000000 )


Links utili
-----------
* [Una guida per intercettare e decifrare la comunicazione tra la app e l'apparecchio](https://www.torsten-traenkner.de/wissen/smarthome/heizung.php)
* [Convertire da HEX a DEC e viceversa](https://www.rapidtables.com/convert/number/hex-to-decimal.html?x=17)




Prepararsi al collegamento Bluethooth
=====================================

Verificare che il Bluethooth del proprio PC non sia impegnato
-------------------------------------------------------------

    rfkill list bluetooth


Elencare i bluethooth disponibili sul PC
----------------------
NB: per Raspberry 1 e 2 è necessario un USB bluetooth stick

    hciconfig -a

Attivare il bluethooth
----------------------

    hciconfig hci0 up

Come ottener il MAC del bluethooth del proprio PC
---------------------------------------------------

    $ hcitool dev
    Devices:
    hci0	DC:A6:32:0F:7E:00

Cerca in aria quali apparecchi BLE sono raggiungibili
-----------------------------------------------------

    $ sudo hcitool -i hci0 lescan
    LE Scan ...
    68:9E:19:00:BD:22 (unknown)
    68:9E:19:00:BD:22 Comet Blue
    ^C

Come sapere chi ha prodotto il pezzo bluethoth
---------------------------------------------------
La prima parte dell'indirizzo MAC "appartiene" ad un produttore.
Al link
[IEEE website](https://regauth.standards.ieee.org/standards-ra-web/pub/view.html#registries)
si può scoprire chi è.

Selezionare  "All MAC" e poi mettere come filtro la prima parte del MAC, senza spazi, 
p.es. "dca632" o "689e19".

* DC:A6:32 -> Raspberry Pi Trading Ltd.
* 68:9E:19 -> Texas Instruments

Farsi dare informazioni di base sulla controparte
-------------------------------------------------

    $ sudo hcitool -i hci0 leinfo 68:9E:19:00:BD:22
    Requesting information ...
	Handle: 64 (0x0040)
	LMP Version: 4.0 (0x6) LMP Subversion: 0x140
	Manufacturer: Texas Instruments Inc. (13)
	Features: 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00


Comunicare con il termostato usando gatttool
============================

Aprire un'interfaccia interattiva 
---------------------------------

    $ sudo gatttool -i hci0 -b 68:9E:19:00:BD:22 -I
    [68:9E:19:00:BD:22][LE]> connect
    Attempting to connect to 68:9E:19:00:BD:22
    Connection successful
    [68:9E:19:00:BD:22][LE]>
    
Listing device services
-----------------------

    [68:9E:19:00:BD:22][LE]> primary
    attr handle: 0x0001, end grp handle: 0x000b uuid: 00001800-0000-1000-8000-00805f9b34fb
    attr handle: 0x000c, end grp handle: 0x000f uuid: 00001801-0000-1000-8000-00805f9b34fb
    attr handle: 0x0010, end grp handle: 0x001a uuid: 0000180a-0000-1000-8000-00805f9b34fb
    attr handle: 0x001b, end grp handle: 0xffff uuid: 47e9ee00-47e9-11e4-8939-164230d1df67
    


la prima parte dopo uuid:

    0X1801: Generic Attribute
    0X1800: Generic Access
    0X180A: Device Information
    47e9ee00: ?????
    
    
List characteristics
-------------------    

Le characteristic properties (char properties) sono descritte da un Byte.
Se il secondo bit (quello del "2") è messo a 1, allora
la caratteristica può essere letta.
Se il quarto bit (quello dell' "8") è messo a 1, allora
la caratteristicha può essere scritta.

Se ci sono entrambi i bit (2+8=10 = 0x0a) allora sono read+write.

    0x02  ------1-  read only
    0x08  ----1---  write only
    0x0a  ----1-1-  read and write
    0x20  --1-----  ???

La tabella complessiva di tale Byte ha i seguenti significati

    0x00  --------  Type unknow
    0x01  -------1  Broadcast of Characteristic value permitted
    0x02  ------1-  Read permitted
    0x04  -----1--  Write without response permitted
    0x08  ----1---  Write permitted
    0x10  ---1----  Notification permitted
    0x20  --1-----  Indications permitted
    0x40  -1------  Signed write permitted
    0x80  1-------  Extended proprieties, definited in the 
                    Charaacteristics Extended Proprieties Descriptor



Generic Attribute (0x1801) 
--------------------------
    [68:9E:19:00:BD:22][LE]> characteristics c f
    handle: 0x000d, char properties: 0x20, char value handle: 0x000e, uuid: 00002a05-0000-1000-8000-00805f9b34fb


char properties = 0x20 , dunque no reading handle

Generic Access (0x1800)
-----------------------
    [68:9E:19:00:BD:22][LE]> characteristics 1 b
    handle: 0x0002, char properties: 0x02, char value handle: 0x0003, uuid: 00002a00-0000-1000-8000-00805f9b34fb
    handle: 0x0004, char properties: 0x02, char value handle: 0x0005, uuid: 00002a01-0000-1000-8000-00805f9b34fb
    handle: 0x0006, char properties: 0x0a, char value handle: 0x0007, uuid: 00002a02-0000-1000-8000-00805f9b34fb
    handle: 0x0008, char properties: 0x08, char value handle: 0x0009, uuid: 00002a03-0000-1000-8000-00805f9b34fb
    handle: 0x000a, char properties: 0x02, char value handle: 0x000b, uuid: 00002a04-0000-1000-8000-00805f9b34fb


Device Name (0x2A00)
--------------------
    [68:9E:19:00:BD:22][LE]> char-read-hnd 3
    Characteristic value/descriptor: 43 6f 6d 65 74 20 42 6c 75 65 

tradotto da utf8 significa (vedasi: https://www.utf8-chartable.de/)
   Comet Blue


Appearance (0x2A01)
-------------------
    [68:9E:19:00:BD:22][LE]> char-read-hnd 5
    Characteristic value/descriptor: 00 00 

???  (0x2A02)
------------------------------
    [68:9E:19:00:BD:22][LE]> char-read-hnd 7
    Characteristic value/descriptor: 00 


???  (0x2A03)
------------------------------
    [68:9E:19:00:BD:22][LE]> char-read-hnd 9
    Error: Characteristic value/descriptor read failed: Attribute can't be read


???  (0x2A04)
------------------------------
    [68:9E:19:00:BD:22][LE]> char-read-hnd a
    Characteristic value/descriptor: 02 0b 00 04 2a 
ovvero
    handle "0b" è in sola lettura ("02") e ha un uuid 2a04

    [68:9E:19:00:BD:22][LE]> char-read-hnd b
    Characteristic value/descriptor: 50 00 a0 00 00 00 e8 03 

ovvero
   Q.......
oppure
   80 0 16 0 0 0 232 3



Device Information (0X180A)
---------------------------

    [68:9E:19:00:BD:22][LE]> characteristics 10 1a
    handle: 0x0011, char properties: 0x02, char value handle: 0x0012, uuid: 00002a23-0000-1000-8000-00805f9b34fb
    handle: 0x0013, char properties: 0x02, char value handle: 0x0014, uuid: 00002a24-0000-1000-8000-00805f9b34fb
    handle: 0x0015, char properties: 0x02, char value handle: 0x0016, uuid: 00002a26-0000-1000-8000-00805f9b34fb
    handle: 0x0017, char properties: 0x02, char value handle: 0x0018, uuid: 00002a28-0000-1000-8000-00805f9b34fb
    handle: 0x0019, char properties: 0x02, char value handle: 0x001a, uuid: 00002a29-0000-1000-8000-00805f9b34fb
       
Device information, MAC? (0X2A23)
----
Pare che contenga il proprio indirizzo MAC, con due Byte vuoti in mezzo.

    [68:9E:19:00:BD:22][LE]> char-read-hnd 12               
    Characteristic value/descriptor: 22 bd 00 00 00 19 9e 68                        "½.....h

model number (0X2A24)
---------------------
    [68:9E:19:00:BD:22][LE]> char-read-hnd 14
    Characteristic value/descriptor: 43 6f 6d 65 74 20 42 6c 75 65                  Comet Blue

firmware revision (0X2A26)
--------------------------
    [68:9E:19:00:BD:22][LE]> char-read-hnd 16
    Characteristic value/descriptor: 43 4f 42 4c 30 31 32 36                        COBL0126

software revision (0X2A28)
-----------------
    [68:9E:19:00:BD:22][LE]> char-read-hnd 18
    Characteristic value/descriptor: 30 2e 30 2e 36 2d 73 79 67 6f 6e 69 78 31      0.0.6-sygonix1

manufacturer name (0X2A29)
--------------------------
    [68:9E:19:00:BD:22][LE]> char-read-hnd 1a
    Characteristic value/descriptor: 45 55 52 4f 74 72 6f 6e 69 63 20 47 6d 62 48   EUROtronic Gmbh
    



????? (47e9ee00)
================
    [68:9E:19:00:BD:22][LE]> characteristics 1b ffff
    handle: 0x001c, char properties: 0x0a, char value handle: 0x001d, uuid: 47e9ee01-47e9-11e4-8939-164230d1df67
    handle: 0x001e, char properties: 0x0a, char value handle: 0x001f, uuid: 47e9ee10-47e9-11e4-8939-164230d1df67
    handle: 0x0020, char properties: 0x0a, char value handle: 0x0021, uuid: 47e9ee11-47e9-11e4-8939-164230d1df67
    handle: 0x0022, char properties: 0x0a, char value handle: 0x0023, uuid: 47e9ee12-47e9-11e4-8939-164230d1df67
    handle: 0x0024, char properties: 0x0a, char value handle: 0x0025, uuid: 47e9ee13-47e9-11e4-8939-164230d1df67
    handle: 0x0026, char properties: 0x0a, char value handle: 0x0027, uuid: 47e9ee14-47e9-11e4-8939-164230d1df67
    handle: 0x0028, char properties: 0x0a, char value handle: 0x0029, uuid: 47e9ee15-47e9-11e4-8939-164230d1df67
    handle: 0x002a, char properties: 0x0a, char value handle: 0x002b, uuid: 47e9ee16-47e9-11e4-8939-164230d1df67
    handle: 0x002c, char properties: 0x0a, char value handle: 0x002d, uuid: 47e9ee20-47e9-11e4-8939-164230d1df67
    handle: 0x002e, char properties: 0x0a, char value handle: 0x002f, uuid: 47e9ee21-47e9-11e4-8939-164230d1df67
    handle: 0x0030, char properties: 0x0a, char value handle: 0x0031, uuid: 47e9ee22-47e9-11e4-8939-164230d1df67
    handle: 0x0032, char properties: 0x0a, char value handle: 0x0033, uuid: 47e9ee23-47e9-11e4-8939-164230d1df67
    handle: 0x0034, char properties: 0x0a, char value handle: 0x0035, uuid: 47e9ee24-47e9-11e4-8939-164230d1df67
    handle: 0x0036, char properties: 0x0a, char value handle: 0x0037, uuid: 47e9ee25-47e9-11e4-8939-164230d1df67
    handle: 0x0038, char properties: 0x0a, char value handle: 0x0039, uuid: 47e9ee26-47e9-11e4-8939-164230d1df67
    handle: 0x003a, char properties: 0x0a, char value handle: 0x003b, uuid: 47e9ee27-47e9-11e4-8939-164230d1df67
    handle: 0x003c, char properties: 0x0a, char value handle: 0x003d, uuid: 47e9ee2a-47e9-11e4-8939-164230d1df67
    handle: 0x003e, char properties: 0x0a, char value handle: 0x003f, uuid: 47e9ee2b-47e9-11e4-8939-164230d1df67
    handle: 0x0040, char properties: 0x0a, char value handle: 0x0041, uuid: 47e9ee2c-47e9-11e4-8939-164230d1df67
    handle: 0x0042, char properties: 0x0a, char value handle: 0x0043, uuid: 47e9ee2d-47e9-11e4-8939-164230d1df67
    handle: 0x0044, char properties: 0x0a, char value handle: 0x0045, uuid: 47e9ee2e-47e9-11e4-8939-164230d1df67
    handle: 0x0046, char properties: 0x08, char value handle: 0x0047, uuid: 47e9ee30-47e9-11e4-8939-164230d1df67
    
   

 Documentazione (forse giusta)
 =============================
        'device_name': {
            'description': 'device name',
            'uuid': '00002a00-0000-1000-8000-00805f9b34fb',
            'decode': _decode_str,
        },

        'model_number': {
            'description': 'model number',
            'uuid': '00002a24-0000-1000-8000-00805f9b34fb',
            'decode': _decode_str,
        },

        'firmware_revision': {
            'description': 'firmware revision',
            'uuid': '00002a26-0000-1000-8000-00805f9b34fb',
            'decode': _decode_str,
        },

        'software_revision': {
            'description': 'software revision',
            'uuid': '00002a28-0000-1000-8000-00805f9b34fb',
            'decode': _decode_str,
        },

        'manufacturer_name': {
            'description': 'manufacturer name',
            'uuid': '00002a29-0000-1000-8000-00805f9b34fb',
            'decode': _decode_str,
        },

        'datetime': {
            'description': 'time and date',
            'uuid': '47e9ee01-47e9-11e4-8939-164230d1df67',
            'read_requires_pin': True,
            'decode': _decode_datetime,
            'encode': _encode_datetime,
        },

        'status': {
            'description': 'status',
            'uuid': '47e9ee2a-47e9-11e4-8939-164230d1df67',
            'read_requires_pin': True,
            'decode': _decode_status,
            'encode': _encode_status,
        },

        'temperatures': {
            'description': 'temperatures',
            'uuid': '47e9ee2b-47e9-11e4-8939-164230d1df67',
            'read_requires_pin': True,
            'decode': _decode_temperatures,
            'encode': _encode_temperatures,
        },

        'battery': {
            'description': 'battery charge',
            'uuid': '47e9ee2c-47e9-11e4-8939-164230d1df67',
            'read_requires_pin': True,
            'decode': _decode_battery,
        },

        'firmware_revision2': {
            'description': 'firmware revision #2',
            'uuid': '47e9ee2d-47e9-11e4-8939-164230d1df67',
            'read_requires_pin': True,
            'decode': _decode_str,
        },

        'lcd_timeout': {
            'description': 'LCD timeout',
            'uuid': '47e9ee2e-47e9-11e4-8939-164230d1df67',
            'read_requires_pin': True,
            'decode': _decode_lcd_timeout,
            'encode': _encode_lcd_timeout,
        },

        'pin': {
            'description': 'PIN',
            'uuid': '47e9ee30-47e9-11e4-8939-164230d1df67',
            'encode': _encode_pin,
        },
    }

    SUPPORTED_TABLE_VALUES = {
        'day': {
            'uuid': '47e9ee10-47e9-11e4-8939-164230d1df67',
            'num': 7,
            'read_requires_pin': True,
            'decode': _decode_day,
            'encode': _encode_day,
        },

        'holiday': {
            'uuid': '47e9ee20-47e9-11e4-8939-164230d1df67',
            'num': 8,
            'read_requires_pin': True,
            'decode': _decode_holiday,
            'encode': _encode_holiday,
        },
    }

        handle              <--UUID-->                     RW PIN  descrizione            esempio
    03  0x0003    00  00002a00-0000-1000-8000-00805f9b34fb R   no  device name            Comet Blue

    05  0x0005    01  00002a01-0000-1000-8000-00805f9b34fb R   no  Generic Access         0x0000
    07  0x0007    02  00002a02-0000-1000-8000-00805f9b34fb RW  no  Generic Access         0x00
    09  0x0009    03  00002a03-0000-1000-8000-00805f9b34fb  W  no  Generic Access         P 00:a0:00:00:00:e8:03 
    0b  0x000b    04  00002a04-0000-1000-8000-00805f9b34fb R   no  Generic Access        
    0e  0x000e    05  00002a05-0000-1000-8000-00805f9b34fb ??  no  Generic Attribute     

    12  0x0012    23  00002a23-0000-1000-8000-00805f9b34fb R   no  Device Information    
    14  0x0014    24  00002a24-0000-1000-8000-00805f9b34fb R   no  model                  Comet Blue
    16  0x0016    26  00002a26-0000-1000-8000-00805f9b34fb R   no  firmware               COBL0126
    18  0x0018    28  00002a28-0000-1000-8000-00805f9b34fb R   no  software               0.0.6-sygonix1
    1a  0x001a    29  00002a29-0000-1000-8000-00805f9b34fb R   no  produttore             EUROtronic Gmbh

Ora attuale
-----------
              ||
              ++   <--UUID-->
    ..  47e9ee..-47e9-11e4-8939-164230d1df67 RW PIN descrizione	esempio (HEX)
    
    01  47e9ee01-47e9-11e4-8939-164230d1df67 RW yes Ora attuale     (17:0e:16:0c:13) precisa al minuto

    byte  esempio (14:23 del 22/12/2019)
     1    (17) {23} minuti
     1    (0e) {14} ora
     1    (16) {22} giorno del mese
     1    (0c) {12} mese
     1    (13) {19} anno (post 2000)

Programmazione per ciascun giorno settimana
-------------------------------------------
Vengono indicati per ciascun giorno 4 "pacchetti" composti da un "inizio" e un "fine".
Ciascun "inizio" o "fine" viene indicato in 10minuti. p.es. 25(hex) diventa 37 che significa 370 minuti ovvero le 06h10'.

Secondo la documentazione ufficiale, dovrebbe essere possibile indicare 8 "pacchetti".
Forse ciò avviene inviando semplicemente 8 pacchetti, e di default gli ultimi 4 sono sempre 00:00:00:00.
Nelle app c'è un flag che dice se usare solo 4 o 8 "pacchetti". 
Ma forse intendono 8 "cambiamenti" e dunque 4 "pacchetti".

              ||
              ++   <--UUID-->
    ..  47e9ee..-47e9-11e4-8939-164230d1df67 RW PIN descrizione	esempio (HEX)
    
    10  47e9ee10-47e9-11e4-8939-164230d1df67 RW yes lunedi          46:7e:00:00:00:00:00:00
    11  47e9ee11-47e9-11e4-8939-164230d1df67 RW yes martedi         24:28:49:7e:00:00:00:00
    12  47e9ee12-47e9-11e4-8939-164230d1df67 RW yes mercoledi       24:28:5d:7e:00:00:00:00
    13  47e9ee13-47e9-11e4-8939-164230d1df67 RW yes giovedi         24:28:00:00:00:00:00:00
    14  47e9ee14-47e9-11e4-8939-164230d1df67 RW yes venerdi         25:36:58:76:00:00:00:00
    15  47e9ee15-47e9-11e4-8939-164230d1df67 RW yes sabato          3a:76:00:00:00:00:00:00
    16  47e9ee16-47e9-11e4-8939-164230d1df67 RW yes domenica        3a:70:00:00:00:00:00:00

Programmazione delle assenze (holiday)
--------------------------------------
È possibile programmare fino a 8 assenze.
È necessario indicare la data e orario di inizio e data e orario di fine.

Il periodo è definito da 2x4 Byte, il nono Byte è per la temperatura di riferimento
  

    Byte 
     1    inizio: ora
     2    inizio: giorno
     3    inizio: mese
     4    inizio: anno (post 2000)
     5    fine  : ora
     6    fine  : giorno
     7    fine  : mese
     8    fine  : anno (post 2000)
     9    temperatura (10°C = 20/2 -> 0x14)

              ||
              ++   <--UUID-->
    ..  47e9ee..-47e9-11e4-8939-164230d1df67 RW PIN descrizione	esempio (HEX)
    
    20  47e9ee20-47e9-11e4-8939-164230d1df67 RW yes Assenza_1	09:07:02:15:09:0d:02:15:14
    21  47e9ee21-47e9-11e4-8939-164230d1df67 RW yes Assenza_2	00:00:00:00:00:00:00:00:00
    22  47e9ee22-47e9-11e4-8939-164230d1df67 RW yes Assenza_3	00:00:00:00:00:00:00:00:00
    23  47e9ee23-47e9-11e4-8939-164230d1df67 RW yes Assenza_4	00:00:00:00:00:00:00:00:00
    24  47e9ee24-47e9-11e4-8939-164230d1df67 RW yes Assenza_5	00:00:00:00:00:00:00:00:00
    25  47e9ee25-47e9-11e4-8939-164230d1df67 RW yes Assenza_6	00:00:00:00:00:00:00:00:00
    26  47e9ee26-47e9-11e4-8939-164230d1df67 RW yes Assenza_7	00:00:00:00:00:00:00:00:00
    27  47e9ee27-47e9-11e4-8939-164230d1df67 RW yes Assenza_8	00:00:00:00:00:00:00:00:00

Altre impostazioni
------------------
              ||
              ++   <--UUID-->
    ..  47e9ee..-47e9-11e4-8939-164230d1df67 RW PIN descrizione	esempio (HEX) {DEC}
    
    2a  47e9ee2a-47e9-11e4-8939-164230d1df67 RW yes status	(00:00:08)
    2b  47e9ee2b-47e9-11e4-8939-164230d1df67 RW yes Temperature (29:28:28:28:00:04:0a) {41,40,40,40,0,4,10}
    2c  47e9ee2c-47e9-11e4-8939-164230d1df67 RW yes Batteria	{43}
    2d  47e9ee2d-47e9-11e4-8939-164230d1df67 RW yes Software	soft cobl0126
    2e  47e9ee2e-47e9-11e4-8939-164230d1df67 RW yes LCD timeout (1e:00) {30,0} (secondi previsti : countdown)


### Status (RW - PIN - 3d 0x003d - 2a 47e9ee2a-47e9-11e4-8939-164230d1df67)


Nel primo Byte il bit delle unità (0x01) indica se è attiva la modalità "manuale" mentre il bit dei 128 (0x80) indica se è attivata
la sicurezza bimbi (ovvero bit=1 significa che anche toccando i tasti, non succede nulla)

    8421 8421 : 8421 8421 : 8421 8421
    |  |    |        |\|/   |||| ||||
    |  |    |        | |    |||| |||+-------------------------------------------------------------
    |  |    |        | |    |||| |
    |  |    |        | |    |||| +-------- 0x08 Ignoto, spesso attivo
    |  |    |        | |    
    |  |    |        | +------------------ 0x07 Valvola:  0x00=000=chiusa, 0x07=111=aperta, 0x06=110=chiudente 0x01=001=aprente
    |  |    |        +-------------------- 0x08 Temperatura da mantenere: 0=presets 1=ad hoc
    |  |    |  
    |  |    +----------------------------- 0x01 Automatismo: 0=AUTOMATIC 1=MANUAL
    |  | 
    |  +---------------------------------- 0x10 Ignoto, forse antigelo
    +------------------------------------- 0x80 Blocco testierino. 1=LOCK 2=UNLOCK

Quando viene chiusa la valvola nel secondo Byte si passa dalle seguenti fasi
 
    07 valvola aperta
    06 valvola in fase di chiusura (per diversi secondi)
    01 valvola in fase di apertura (per una frazione di secondo)
    00 valvola chiusa

Nella fase di apertura invece la sequenza per il secondo byte è quella intuitiva

    00 valvola chiusa
    01 valvola in fase di apertura
    07 valvola aperta

#### scrivere nel primo Byte
Pare che attivando singoli bit nel primo Byte i possano dare ordini

    ---- ---1 010000 attiva    il seguente bit del primo Byte ---- ---X   X: 1=MANUAL 0=AUTOMATIC
    ---- --1- 020000 disattiva il seguente bit del primo Byte ---- ---X   X: 1=MANUAL 0=AUTOMATIC
    ---- -1-- 040000 apre   la valvola (nel secondo Byte si può leggere la situazione della valvola)
    ---- 1--- 080000 chiude la valvola
    ---1 ---- 100000 disattiva il seguente bit del primo Byte ---X ----   X: ignoto (forse "antigelo" ?)
    --1- ---- 200000 attiva    il seguente bit del primo Byte ---X ----   X: ignoto (forse "antigelo" ?)
    -1-- ---- 400000 disattiva il seguente bit del primo Byte X--- ----   X: 1=LOCK 0=UNLOCK
    1--- ---- 800000 attiva    il seguente bit del primo Byte X--- ----   X: 1=LOCK 0=UNLOCK 

Esempio, in cui si attiva il Boost (o semplicemente si da ordine di aprire la valvola) e si impostano i bit X--X---- del primo Byte 
   
    char-write-req 3d a40000

che corrisponde al primo byte

    1010 0100

comporta il seguente risultato

    char-read.hnd 3d
    > 900108
    char-read.hnd 3d
    > 900108
    char-read.hnd 3d
    > 900108
    char-read.hnd 3d
    > 900708

e chiudendo la valvola e disattivando i bit di cui sopra (o1o1 1ooo)
    char-write-req 3d 580000
    char-read-hnd 3d
    > 00 07 08 
    char-read-hnd 3d
    > 00 06 08 
    char-read-hnd 3d
    > 00 06 08 
    char-read-hnd 3d
    > 00 01 08 
    char-read-hnd 3d
    > 00 00 08 


    

#### Boost - apertura/chiusura esplicita della valvola

* scrivendo nel primo byte un valore che ha il bit di 04 attivato, apre la valvola
* scrivendo nel primo byte un valore che ha il bit di 08 attivato, chiude la valvola


### Impostazione delle temperature (RW - PIN - 3f 0x003f - 2b 47e9ee2b-47e9-11e4-8939-164230d1df67)

    Byte  descrizione
     1    Temperatura attuale     (in 1/2 gradi, dunque precisione i 0,5°C)
     2    Temperatura desiderata  (in 1/2 gradi, dunque precisione i 0,5°C)
     3    Temperatura preimpostata come "notte", "eco",... (in 1/2 gradi, dunque precisione i 0,5°C)
     4    Temperatura preimpostata come "giorno", "confort",... (in 1/2 gradi, dunque precisione i 0,5°C)
     5    Offset temperatura (in 1/2 gradi, dunque precisione i 0,5°C)
     6    "Finestra aperta": sensibilità (04, 08, 0c)
     7    "Finestra aperta": durata (in minuti)

Leggere i valori
 char-read-hnd 0x003f

    2d 2a 24 2a 00 04 0a
     |  |  |  |  |  |  |
     |  |  |  |  |  |  window open minutes
     |  |  |  |  |  window open detection
     |  |  |  |  offset temperature   (0x01 =  1 ->  1/2 =  0.5 °C)
     |  |  |  target temperature high (0x2a = 42 -> 42/2 = 21.0 °C)
     |  |  target temperature low (0x24 = 36 -> 36/2 = 18.0 °C)
     |  temperature for manual mode (0x2a = 42 -> 42/2 = 21.0 °C)
     current temperature (0x2d = 45 -> 45/2 = 22.5 °C)

impostare nuovi valori. Per i campi non utilizzati, inviare 0x80

     char-write-req 0x003f 8080242a008080

dove
 
     80 80 24 2a 00 80 80
            |  |  |
            |  |  offset temperature
            |  target temperature high (0x2a == 21.0 °C)
            target temperature low (0x24 == 18.0 °C)

Autenticazione (PIN)
-----------------------
Bisogna convertire il numero (di 6 cifre) in esadecimale.
A seconda della programmazione, bisogna invertire la sequenza dei byte.
p.es. 764491 diventa BAA4B ovvero 00:0B:AA:4B e invertendo i byte diventa 4BAA0B00

Quando si tolgono le batterie, il PIN viene reimpostato a 000000.

Forse per impostare un nuovo PIN serve la app, almeno fino a 
quando non si decifra la comunicazione durante la fase di 
impostazione di un nuovo PIN.

              ||
              ++   <--UUID-->
    ..  47e9ee..-47e9-11e4-8939-164230d1df67 RW PIN descrizione	esempio (HEX)
    
    30  47e9ee30-47e9-11e4-8939-164230d1df67  W	yes PIN

Impostare da dentro gatttool

    char-write-req 47 4baa0b00

Esempi Python
=============

Sessione minima
---------------

Con Python3

    manolo@huge:~ $ python3
    Python 3.7.3 (default, Apr  3 2019, 05:39:12) 
    [GCC 8.2.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.

Import della libreria pygatt (evtl. sudo pip3 install pygatt)

    >>> import pygatt

Connessione (con timeout di 60 secondi

    >>> adapter = pygatt.GATTToolBackend()
    >>> adapter.start()
    >>> device = adapter.connect('68:9E:19:00:BD:22' , 60 )

Chiedo un valore ottenibile anche senza PIN    

    >>> print( device.char_read("00002a29-0000-1000-8000-00805f9b34fb"))
    bytearray(b'EUROtronic GmbH')

Mando il PIN e chiedo un valore (orario) ottenibile solo con PIN    

    >>> device.char_write("47e9ee30-47e9-11e4-8939-164230d1df67" , bytearray([0x4b, 0xaa,0x0b,0x00]) )
    >>> [hex(c) for c in device.char_read("47e9ee01-47e9-11e4-8939-164230d1df67")]
    ['0x28', '0xe', '0x16', '0xc', '0x13']

Chiudo il collegamento    

    >>> adapter.stop()
    
