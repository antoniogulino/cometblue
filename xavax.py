#!/usr/bin/python3
#       Valgono le seguenti regole
#       tutti i parametri scritti in MAIUSCOLO, modificano qualcosa al termostato
#       mentre i parametri scritti in minuscolo non modificano nulla al termostato
# 
# OK    accettare parametri per connessione
#       ===================================
# OK    mac  (-m --mac='12:34:56:78:90:ab'  )
# OK    pin  (-p --pin=123456               )
#
# OK    restituisce informazioni sul termostato
#       =======================================
# OK         mac, gateway (host, user, script)
# OK         nome, fw, sw, ecc
#
#       definisce formato output
#       ========================
# OK       json   -f --format json  --json (default)
#
#       restitutire valori
#       ==================
# OK       ora interna
# OK       LCD timeout
#      
# OK       temperatura misurata
# OK       temperatura target attuale
# OK       temperatura preset comfort/giorno
# OK       temperatura preset eco/risparmio/notte
# OK       temperatura offset
#
# OK       comfort: periodi per singolo giorno settimana
#     
# OK       assenza/vacanze
# OK       temperatura preset assenza/vacanze (per singolo periodo assenza)
#
#       impostare valori
#       ================
# OK       impostare nuovo pin                      --PIN=<6cifre>
# OK       ora esatta                               --CLOCK    imposta l'ora esatta prelevata dal computer
#
# OK       aprire/chiudere direttamente la valvola  --ON --OFF
#
# OK       passare al manuale o automatico        --MANUAL --AUTOMATIC
# OK       bloccare o sbloccare tastierino        --LOCK   --UNLOCK
#
# OK       LCD timeout   --LCD=<valore>  p.es. -L60 o -L 60 o --lcd=60
#
# OK       temperatura target attuale             --NOW=<valore tra 8 e 28>       a passi di 0.5°C
# OK       temperatura preset eco/risparmio/notte --LOW=<valore tra 8 e 28>       a passi di 0.5°C
# OK       temperatura preset comfort/giorno      --HIGH=<valore tra 8 e 28>      a passi di 0.5°C
# OK       temperatura offset                     --OFFSET=<valore tra -5 e +5>   a passi di 0.5°C
# OK       temperatura finestra sensibilita       --WSENSITIVITY=<valore tra 1 e 3>    1=low  2=mid  3=high (di quanto deve essere lo sbalzo e in quanto tempo)
# OK       temperatura finestra durata            --WTIME=<valore tra 5 e 99>    minuti (per quanti minuti la caldaia rimarrà chiusa)
#
#          periodo comfort
#          ---------------
# OK       giorno della settimana a scelta    --LUN=DEL --MAR="06:30-07:20" --MER="6:30-7:20 12:30-14:10" --OGGI="06:30-07:20" --GIO=SCOLASTICO
# OK       giorno della settimana come oggi   --TODAY=     --OGGI=     --HEUTE=
# OK       giorno della settimana come domani --TOMORROW=  --DOMANI=   --MORGEN=
# OK       tutti i giorni uguale              --DAYS=     p.es. --DAYS=SCOLASTICO oppure --DAYS=DEL
#
#          periodo assenza  --HOLIDAY --H1= --H2= --H3= ... --H8=
#          ---------------
# OK       scegliere uno slot specifico e cancellarlo  --H3=DEL   (se esiste, allora lo cancella)
# OK       scegliere uno slot specifico e pulirlo      --H3=CLEAN (se esiste ed è scaduto, allora lo cancella)
# OK       scegliere uno slot specifico e impostare    --H3="30-12-2019h10 31-12-2019h16 16" oppure "NOW TOMORROW 16" oppure "NOW" oppure "DOMANI" oppure "30-12-2019 DOMANI 12"
# OK       cancellare tutt gli slot esistenti          --HOLIDAY=DEL
# OK       cancellare tutt gli slot scaduti            --HOLIDAY=CLEAN
# OK       impostare senza indicare uno slot specifico --HOLIDAY="30-12-2019h10 31-12-2019h16 16" o comunque un "formato Holiday"
#
# OK    mqtt
#       ====
# OK       --mqtt        # se usato, allora pubblica presso il broker
# OK       --broker=     # default --broker=openhab
# OK       --topic=      # default --topic=<MAC-ADDR>
# OK       --port=       # default --port=1883
#
#       Varie
#       =====
# OK       --description  aggiunge il testo nel json
# OK       --log          Si può decidere se nell'output si vuole un log della procedura  --log
#
# TODO  restitutire valori
#       ==================
# 
# TODO     valvola grado di apertura (forse non esiste)
# TODO     farsi dire che sono state impostate HOLI direttamente sul termostato
# TODO     farsi dire quando scade HOLI impostato direttamente sul termostato
# 
# 
# TODO  impostare valori
#       ================
#
# TODO     temperatura preset assenza/vacanze
# 
# TODO     periodo comfort
#          ---------------
# TODO         solo per il giorno di oggi (diverso da --OGGI che sceglie oggi come giorno della settimana da impostare)
# TODO         solo per il giorno di domani (diverso da --DOMANI che sceglie domani come giorno della settimana da impostare)
# TODO         giorno di calendario preciso
# TODO         slot aggiungere
# TODO         slot togliere
# TODO         slot sostituire
# TODO         ora inizio
# TODO         ora fine
# 
# TODO     periodo assenza  --HOLIDAY --H1= --H2= --H3= ... --H8=
#          ---------------
# TODO         giorno corrente
# TODO         giorno domani
# TODO         giorno inizio
# TODO         giorno durata
# TODO         giorno fine
# TODO         slot aggiungere
# TODO         slot togliere
# TODO         slot sostituire
# 
# TODO  formato output
#       ==============
# TODO     testo  -f --format txt
# TODO     tsv    -f --format tsv
#
# TODO  simulazione
#       ===========
# TODO     -s --simulation 


#manolo@huge:~ $ sudo gatttool -i hci0 -b 68:9E:19:00:BD:22 -I
#[68:9E:19:00:BD:22][LE]> connect
#Attempting to connect to 68:9E:19:00:BD:22
#Connection successful
#[68:9E:19:00:BD:22][LE]> characteristics
#handle: 0x0002, char properties: 0x02, char value handle: 0x0003, uuid: 00002a00-0000-1000-8000-00805f9b34fb
#handle: 0x0004, char properties: 0x02, char value handle: 0x0005, uuid: 00002a01-0000-1000-8000-00805f9b34fb
#handle: 0x0006, char properties: 0x0a, char value handle: 0x0007, uuid: 00002a02-0000-1000-8000-00805f9b34fb
#handle: 0x0008, char properties: 0x08, char value handle: 0x0009, uuid: 00002a03-0000-1000-8000-00805f9b34fb
#handle: 0x000a, char properties: 0x02, char value handle: 0x000b, uuid: 00002a04-0000-1000-8000-00805f9b34fb
#handle: 0x000d, char properties: 0x20, char value handle: 0x000e, uuid: 00002a05-0000-1000-8000-00805f9b34fb
#handle: 0x0011, char properties: 0x02, char value handle: 0x0012, uuid: 00002a23-0000-1000-8000-00805f9b34fb
#handle: 0x0013, char properties: 0x02, char value handle: 0x0014, uuid: 00002a24-0000-1000-8000-00805f9b34fb
#handle: 0x0015, char properties: 0x02, char value handle: 0x0016, uuid: 00002a26-0000-1000-8000-00805f9b34fb
#handle: 0x0017, char properties: 0x02, char value handle: 0x0018, uuid: 00002a28-0000-1000-8000-00805f9b34fb
#handle: 0x0019, char properties: 0x02, char value handle: 0x001a, uuid: 00002a29-0000-1000-8000-00805f9b34fb
#handle: 0x001c, char properties: 0x0a, char value handle: 0x001d, uuid: 47e9ee01-47e9-11e4-8939-164230d1df67
#handle: 0x001e, char properties: 0x0a, char value handle: 0x001f, uuid: 47e9ee10-47e9-11e4-8939-164230d1df67
#handle: 0x0020, char properties: 0x0a, char value handle: 0x0021, uuid: 47e9ee11-47e9-11e4-8939-164230d1df67
#handle: 0x0022, char properties: 0x0a, char value handle: 0x0023, uuid: 47e9ee12-47e9-11e4-8939-164230d1df67
#handle: 0x0024, char properties: 0x0a, char value handle: 0x0025, uuid: 47e9ee13-47e9-11e4-8939-164230d1df67
#handle: 0x0026, char properties: 0x0a, char value handle: 0x0027, uuid: 47e9ee14-47e9-11e4-8939-164230d1df67
#handle: 0x0028, char properties: 0x0a, char value handle: 0x0029, uuid: 47e9ee15-47e9-11e4-8939-164230d1df67
#handle: 0x002a, char properties: 0x0a, char value handle: 0x002b, uuid: 47e9ee16-47e9-11e4-8939-164230d1df67
#handle: 0x002c, char properties: 0x0a, char value handle: 0x002d, uuid: 47e9ee20-47e9-11e4-8939-164230d1df67
#handle: 0x002e, char properties: 0x0a, char value handle: 0x002f, uuid: 47e9ee21-47e9-11e4-8939-164230d1df67
#handle: 0x0030, char properties: 0x0a, char value handle: 0x0031, uuid: 47e9ee22-47e9-11e4-8939-164230d1df67
#handle: 0x0032, char properties: 0x0a, char value handle: 0x0033, uuid: 47e9ee23-47e9-11e4-8939-164230d1df67
#handle: 0x0034, char properties: 0x0a, char value handle: 0x0035, uuid: 47e9ee24-47e9-11e4-8939-164230d1df67
#handle: 0x0036, char properties: 0x0a, char value handle: 0x0037, uuid: 47e9ee25-47e9-11e4-8939-164230d1df67
#handle: 0x0038, char properties: 0x0a, char value handle: 0x0039, uuid: 47e9ee26-47e9-11e4-8939-164230d1df67
#handle: 0x003a, char properties: 0x0a, char value handle: 0x003b, uuid: 47e9ee27-47e9-11e4-8939-164230d1df67
#handle: 0x003c, char properties: 0x0a, char value handle: 0x003d, uuid: 47e9ee2a-47e9-11e4-8939-164230d1df67
#handle: 0x003e, char properties: 0x0a, char value handle: 0x003f, uuid: 47e9ee2b-47e9-11e4-8939-164230d1df67
#handle: 0x0040, char properties: 0x0a, char value handle: 0x0041, uuid: 47e9ee2c-47e9-11e4-8939-164230d1df67
#handle: 0x0042, char properties: 0x0a, char value handle: 0x0043, uuid: 47e9ee2d-47e9-11e4-8939-164230d1df67
#handle: 0x0044, char properties: 0x0a, char value handle: 0x0045, uuid: 47e9ee2e-47e9-11e4-8939-164230d1df67
#handle: 0x0046, char properties: 0x08, char value handle: 0x0047, uuid: 47e9ee30-47e9-11e4-8939-164230d1df67


import sys, getopt, os
import pygatt
import socket
import getpass
import json
import struct
import datetime
import paho.mqtt.client as mqtt

#####
adapter = pygatt.GATTToolBackend()

DAYS_IT = ['oggi', 'lunedi', 'martedi', 'mercoledi', 'giovedi', 'venerdi', 'sabato', 'domenica']
DAYS_DE = ['Heute', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag']
DAYS_EN = ['today', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
if os.getenv('LANG')[0:2] == 'it' :
    DAYS = DAYS_IT
elif os.getenv('LANG')[0:2] == 'de' :
    DAYS = DAYS_DE
else:    
    DAYS = DAYS_EN

TMP_asByte = { 
     'PROGRAM_SETTINGS'        : []   # [ None , None , None ]
    ,'PROGRAM_SETTINGS_before' : []   # [ None , None , None ]
}


RISPOSTA = { 'gateway' : {'host' : socket.gethostname(), 'param' : ' '.join(sys.argv), 'description' : None, 'script' : __file__ , 'user' : getpass.getuser()}
               , 'addLOG' : False
               , 'LOG' : []
               , 'device' : {'mac':'', 'pin' : False}
               }
               
#RISPOSTA['LOG'] = []

adesso = datetime.datetime.now()  
RISPOSTA['gateway']['datetime'] = "%4d-%02d-%02d %02d:%02d:%02d" % (adesso.year , adesso.month, adesso.day, adesso.hour, adesso.minute, adesso.second)
adesso = adesso  + datetime.timedelta(hours=1) 


#############################################################
#   funzioni varie
#        scolastico(day)
#     +  scriviBT(device, uuid, parametro, commento='')
#        leggiBT(device, uuid , commento='')
#        addLog(prefix, testo, commento='') 
#        isFreeHolidaySlot(slot_nr)
#        getFreeHolidaySlots()
#        HolidayKeywordToString(parola, fase)
#        strToHoliday(stringa) 
#        HolidayToBytearray(stringa) TODO
#        HolidayToValue(stringa)     TODO
#     +  deleteHoliday(device,slotNr) 
#     +  cleanHoliday(device, slotNr) 
#        get_device_name(device)
#        get_device_ID(device)
#        get_device_model(device) 
#        get_device_firmware(device)
#        get_device_software(device)
#        get_device_manufacturer(device) 
#        get_device_software2(device)
#        get_device_battery(device) 
#        get_device_datetime(device)
#     +  set_device_datetime(device)
#        get_device_LCD(device)
#        get_program_settings(device)
#        program_settings_to_dict(trebyte)
#        get_program_temp(device)
#        get_program_days( device, day_nr ) 
#        get_program_holidays( device , holi_nr )
#        pinIsOK()
#     +  set_pinIsOK(booleano) 
#        encode_pin(pin)
#        read_bit(byte,pos)
#        help()
#     +  connect(mac,pin)
#        disconnect(commento)
#        main(argv)


#############################################################

def scolastico(day) :
    # 1 = lunedi .... 7 = domenica
    if   day == 1 : risposta =             '12:00-20:30'
    elif day == 2 : risposta = '05:45-06:45 13:00-20:30'
    elif day == 3 : risposta = '06:00-06:45 12:00-20:30'
    elif day == 4 : risposta = '05:45-06:45'
    else : risposta = ''
    return(risposta)




#############################################################

def scriviBT(device, uuid, parametro, commento='') :
    if (pinIsOK() or uuid in ('47e9ee30-47e9-11e4-8939-164230d1df67')):
        if uuid == '47e9ee30-47e9-11e4-8939-164230d1df67' :
            addLog("scriviBT( %1s , ...)" % uuid , 'Provo a immettere PIN',commento) 
        try :
            #device.char_write( uuid , parametro)
            device.char_write( uuid , parametro)
            #':'.join(["%02X" % a for a in bytearray(b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')])
            addLog("scriviBT( %1s , ...)" % uuid , "OK pare che abbia funzionato" ,commento)
            return(1)
        except :
            addLog("scriviBT( %1s , ...)" % uuid , "ERROR pare che non sia riuscito a scrivere" ,commento)
            return(0)
    else :
        return(0)

#############################################################

def leggiBT(device, uuid , commento='') :
    r_uuid_without_pin = ('00002a00-0000-1000-8000-00805f9b34fb'
                          ,'00002a23-0000-1000-8000-00805f9b34fb'
                          ,'00002a24-0000-1000-8000-00805f9b34fb'
                          ,'00002a26-0000-1000-8000-00805f9b34fb'
                          ,'00002a28-0000-1000-8000-00805f9b34fb'
                          ,'00002a29-0000-1000-8000-00805f9b34fb'

                          ,'00002a01-0000-1000-8000-00805f9b34fb'
                          ,'00002a02-0000-1000-8000-00805f9b34fb'
                          ,'00002a04-0000-1000-8000-00805f9b34fb'
                          )
    r_uuid_with_pin = (
                     '47e9ee01-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee10-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee11-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee12-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee13-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee14-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee15-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee16-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee20-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee21-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee22-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee23-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee24-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee25-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee26-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee27-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee2a-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee2b-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee2c-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee2d-47e9-11e4-8939-164230d1df67'
                    ,'47e9ee2e-47e9-11e4-8939-164230d1df67'
                    )
    if pinIsOK() or (uuid in r_uuid_without_pin):
        if (uuid in r_uuid_without_pin) or (pinIsOK() and (uuid in r_uuid_with_pin) ):
            try :
                risposta = device.char_read( uuid )
            except :
                addLog("leggiBT( ) ERROR" , "device.char_read( %1s ) ha generato errore" % uuid, commento)
                if uuid in r_uuid_with_pin :
                    set_pinIsOK(False)
                return([])
                
            addLog("leggiBT( %1s )" % uuid , ':'.join(["%02X" % a for a in risposta]), commento)
            return(risposta)
        else : 
            addLog("leggiBT() ERROR" , "per uuid '%1s' non è prevista la lettura" % uuid, commento)
            return( [] )

    else :
        addLog("leggiBT() ERROR","PIN non immesso e uuid '%1s' non leggibile senza PIN" % uuid, commento)
        return( [] )

#############################################################

#handle: 0x0002, char properties: 0x02, char value handle: 0x0003, uuid: 00002a00-0000-1000-8000-00805f9b34fb
#handle: 0x0004, char properties: 0x02, char value handle: 0x0005, uuid: 00002a01-0000-1000-8000-00805f9b34fb
#handle: 0x0006, char properties: 0x0a, char value handle: 0x0007, uuid: 00002a02-0000-1000-8000-00805f9b34fb
#handle: 0x0008, char properties: 0x08, char value handle: 0x0009, uuid: 00002a03-0000-1000-8000-00805f9b34fb
#handle: 0x000a, char properties: 0x02, char value handle: 0x000b, uuid: 00002a04-0000-1000-8000-00805f9b34fb
#handle: 0x000d, char properties: 0x20, char value handle: 0x000e, uuid: 00002a05-0000-1000-8000-00805f9b34fb
#handle: 0x0011, char properties: 0x02, char value handle: 0x0012, uuid: 00002a23-0000-1000-8000-00805f9b34fb
#handle: 0x0013, char properties: 0x02, char value handle: 0x0014, uuid: 00002a24-0000-1000-8000-00805f9b34fb
#handle: 0x0015, char properties: 0x02, char value handle: 0x0016, uuid: 00002a26-0000-1000-8000-00805f9b34fb
#handle: 0x0017, char properties: 0x02, char value handle: 0x0018, uuid: 00002a28-0000-1000-8000-00805f9b34fb
#handle: 0x0019, char properties: 0x02, char value handle: 0x001a, uuid: 00002a29-0000-1000-8000-00805f9b34fb
#handle: 0x001c, char properties: 0x0a, char value handle: 0x001d, uuid: 47e9ee01-47e9-11e4-8939-164230d1df67
#handle: 0x001e, char properties: 0x0a, char value handle: 0x001f, uuid: 47e9ee10-47e9-11e4-8939-164230d1df67
#handle: 0x0020, char properties: 0x0a, char value handle: 0x0021, uuid: 47e9ee11-47e9-11e4-8939-164230d1df67
#handle: 0x0022, char properties: 0x0a, char value handle: 0x0023, uuid: 47e9ee12-47e9-11e4-8939-164230d1df67
#handle: 0x0024, char properties: 0x0a, char value handle: 0x0025, uuid: 47e9ee13-47e9-11e4-8939-164230d1df67
#handle: 0x0026, char properties: 0x0a, char value handle: 0x0027, uuid: 47e9ee14-47e9-11e4-8939-164230d1df67
#handle: 0x0028, char properties: 0x0a, char value handle: 0x0029, uuid: 47e9ee15-47e9-11e4-8939-164230d1df67
#handle: 0x002a, char properties: 0x0a, char value handle: 0x002b, uuid: 47e9ee16-47e9-11e4-8939-164230d1df67
#handle: 0x002c, char properties: 0x0a, char value handle: 0x002d, uuid: 47e9ee20-47e9-11e4-8939-164230d1df67
#handle: 0x002e, char properties: 0x0a, char value handle: 0x002f, uuid: 47e9ee21-47e9-11e4-8939-164230d1df67
#handle: 0x0030, char properties: 0x0a, char value handle: 0x0031, uuid: 47e9ee22-47e9-11e4-8939-164230d1df67
#handle: 0x0032, char properties: 0x0a, char value handle: 0x0033, uuid: 47e9ee23-47e9-11e4-8939-164230d1df67
#handle: 0x0034, char properties: 0x0a, char value handle: 0x0035, uuid: 47e9ee24-47e9-11e4-8939-164230d1df67
#handle: 0x0036, char properties: 0x0a, char value handle: 0x0037, uuid: 47e9ee25-47e9-11e4-8939-164230d1df67
#handle: 0x0038, char properties: 0x0a, char value handle: 0x0039, uuid: 47e9ee26-47e9-11e4-8939-164230d1df67
#handle: 0x003a, char properties: 0x0a, char value handle: 0x003b, uuid: 47e9ee27-47e9-11e4-8939-164230d1df67
#handle: 0x003c, char properties: 0x0a, char value handle: 0x003d, uuid: 47e9ee2a-47e9-11e4-8939-164230d1df67
#handle: 0x003e, char properties: 0x0a, char value handle: 0x003f, uuid: 47e9ee2b-47e9-11e4-8939-164230d1df67
#handle: 0x0040, char properties: 0x0a, char value handle: 0x0041, uuid: 47e9ee2c-47e9-11e4-8939-164230d1df67
#handle: 0x0042, char properties: 0x0a, char value handle: 0x0043, uuid: 47e9ee2d-47e9-11e4-8939-164230d1df67
#handle: 0x0044, char properties: 0x0a, char value handle: 0x0045, uuid: 47e9ee2e-47e9-11e4-8939-164230d1df67
#handle: 0x0046, char properties: 0x08, char value handle: 0x0047, uuid: 47e9ee30-47e9-11e4-8939-164230d1df67
        
     

#############################################################
def addLog(prefix, testo, commento='') :
    if commento == '':
        try : 
            risposta = "[ %-40s ] : %1s" % (prefix, testo)
        except :
            print("ERRORE #337")
            print(prefix)
            print(testo)
            print(commento)
            print(RISPOSTA)
            sys.exit()
    else :
        try :
            risposta = "[ %-40s ] : %1s : <%1s>" % (prefix, testo, commento)
        except : 
            print("ERRORE #345")
            print(prefix)
            print(testo)
            print(commento)
            print(RISPOSTA)
            sys.exit()

    if RISPOSTA['addLOG'] :
        try :
            RISPOSTA['LOG'].append(risposta)
        except :
            print("ERRORE #354")
            print(prefix)
            print(testo)
            print(commento)
            print(RISPOSTA)
            sys.exit()

    return(risposta)

#############################################################
def isFreeHolidaySlot(slot_nr) :
    tmp_Holiday_n = "Holiday_%1d" % slot_nr
    return not(tmp_Holiday_n in RISPOSTA['program']['HOLIDAYS'])

#############################################################
def getFreeHolidaySlots() :
    risposta = []
    for tmp_slot_nr in (1,2,3,4,5,6,7,8) :
        if isFreeHolidaySlot(tmp_slot_nr) :
            risposta.append(tmp_slot_nr)
    return(risposta)

#############################################################

def HolidayKeywordToString(parola, fase) :
    tmp_start_hour_default = 7
    tmp_end_hour_default   = 23

    tmp_hour = 0
    tmp_day  = 0
    tmp_month = 0
    tmp_year  = 0
    if parola in ('OGGI','HEUTE','TODAY','NOW','ADESSO') :
        tmp_day   = adesso.day
        tmp_month = adesso.month
        tmp_year  = adesso.year 

        if fase in ('START','INIZIO') :
            if parola in ('NOW','ADESSO') :
                tmp_hour =  adesso.hour
            else :
                tmp_hour =  max( adesso.hour , tmp_start_hour_default )
                
        elif fase in ('END','FINE') :
            tmp_hour  = tmp_end_hour_default 
        parola = "%4d-%02d-%02dh%02d" % (tmp_year, tmp_month, tmp_day, tmp_hour)

    elif parola in ('DOMANI','TOMORROW','MORGEN') :
        tmp_domani = adesso + datetime.timedelta(days=1)
        tmp_day   = tmp_domani.day
        tmp_month = tmp_domani.month
        tmp_year  = tmp_domani.year 

        if fase in ('START','INIZIO') :
            tmp_hour =  max( tmp_domani.hour , tmp_start_hour_default )
                
        elif fase in ('END','FINE') :
            tmp_hour  = tmp_end_hour_default 
        parola = "%4d-%02d-%02dh%02d" % (tmp_year, tmp_month, tmp_day, tmp_hour)

        addLog( 'HolidayKeywordToString() #212' ,  "HolidayKeywordToString( '%1s' , '%1s' ) => '%1s'" % (parola , fase ,parola) )
    return(parola)

#############################################################

def strToHoliday(stringa) :
    tmp_holi = stringa.split() 
    
    NA = -10
    tmp_start_hour  = NA # 0
    tmp_start_day   = NA # 1
    tmp_start_month = NA # 2
    tmp_start_year  = NA # 3
    tmp_end_hour  = NA   # 4
    tmp_end_day   = NA   # 5
    tmp_end_month = NA   # 6 
    tmp_end_year  = NA   # 7 
    tmp_target_temp = NA # 8


    tmp_addLog_prefix = 'strToHoliday()'
    tmp_addLog_commento = stringa

    tmp_start_hour_default = 7
    tmp_end_hour_default   = 23

    if isinstance(RISPOSTA['program']['TEMP']['notte'], dict) :
        tmp_target_temp_default = round(RISPOSTA['program']['TEMP']['notte']['after'] * 2)
    else: 
        tmp_target_temp_default = round(RISPOSTA['program']['TEMP']['notte'] * 2)

    addLog( tmp_addLog_prefix + ' #182' ,  "String ricevuta = '" + stringa + "'" , tmp_addLog_commento )

    if len(tmp_holi)==1 : # vuol dire che ho indicato soltanto il giorno
        tmp_holi = [tmp_holi[0] , tmp_holi[0], "%3.1f" % (tmp_target_temp_default / 2)]
        addLog( tmp_addLog_prefix + ' #190' ,  'è stato indicato soltanto un giorno (di avvio e dunque di arresto)'  , tmp_addLog_commento )
        addLog( tmp_addLog_prefix + ' #191' ,  "2 la richiesta adesso è '%1s %1s %1s'" % (tmp_holi[0],tmp_holi[1],tmp_holi[2])  , tmp_addLog_commento  )

    if len(tmp_holi)==2 and ((tmp_holi[1] in ('DOMANI','TOMORROW',tmp_holi[0])) or (len(tmp_holi[1].split('-'))>2)) :
        tmp_holi = [tmp_holi[0] , tmp_holi[1], "%3.1f" % (tmp_target_temp_default / 2)]
        addLog( tmp_addLog_prefix + ' #195' ,  'è stato indicato soltanto INIZIO e FINE, la temperatura ce la metto io'  , tmp_addLog_commento )

    elif len(tmp_holi)==2 and not((tmp_holi[1] in ('DOMANI','TOMORROW',tmp_holi[0])) or (len(tmp_holi[1].split('-'))>2)):
        tmp_holi = [tmp_holi[0] , tmp_holi[0], tmp_holi[1]] 
        addLog( tmp_addLog_prefix + ' #199' ,  'è stato indicato soltanto un giorno (di avvio e dunque di arresto) e la temperatura' , tmp_addLog_commento  )


    addLog( tmp_addLog_prefix + ' #202' ,  "3 la richiesta adesso è '%1s %1s %1s'" % (tmp_holi[0],tmp_holi[1],tmp_holi[2])  , tmp_addLog_commento  )

    if tmp_holi[1] in ('NOW','ADESSO'):
        tmp_holi[1] = 'OGGI'
        addLog( tmp_addLog_prefix + ' #735' ,  'modifico il secondo paramtero da NOW/ADESSO a OGGI/TODAY'  , tmp_addLog_commento )


    addLog( tmp_addLog_prefix + ' #738' ,  "4 la richiesta adesso è '%1s %1s %1s'" % (tmp_holi[0],tmp_holi[1],tmp_holi[2])  , tmp_addLog_commento  )

    # sostituisco le parole chiave
    tmp_holi[0] = HolidayKeywordToString(tmp_holi[0] ,'START')
    tmp_holi[1] = HolidayKeywordToString(tmp_holi[1] ,'END')

    addLog( tmp_addLog_prefix + ' #259' ,  "5 Sostituisco le parole chiave"  , tmp_addLog_commento  )
    addLog( tmp_addLog_prefix + ' #260' ,  "5 la richiesta adesso è '%1s %1s %1s'" % (tmp_holi[0],tmp_holi[1],tmp_holi[2])  , tmp_addLog_commento  )

    if len(tmp_holi[0].split('-')) == 3 : #è stata indicata almeno una data
        if len(tmp_holi[0].split('h')) == 2 : # è stata indicata anche l'ora
            tmp_start_hour  = int(tmp_holi[0].split('h')[1])
            tmp_data = tmp_holi[0].split('h')[0].split('-')
            if float(tmp_data[0]) > 2000 :
                tmp_start_day   = int(tmp_data[2])
                tmp_start_month = int(tmp_data[1])
                tmp_start_year  = round(float(tmp_data[0]) - 2000)
            else :
                tmp_start_day   = int(tmp_data[0])
                tmp_start_month = int(tmp_data[1])
                tmp_start_year  = round(float(tmp_data[2]) - 2000)
        else :
            tmp_start_hour  = tmp_start_hour_default
            tmp_data = tmp_holi[0].split('h')[0].split('-')
            if float(tmp_data[0]) > 2000 :
                tmp_start_day   = int(tmp_data[2])
                tmp_start_month = int(tmp_data[1])
                tmp_start_year  = round(float(tmp_data[0]) - 2000)
            else :
                tmp_start_day   = int(tmp_data[0])
                tmp_start_month = int(tmp_data[1])
                tmp_start_year  = round(float(tmp_data[2]) - 2000)
       

    if len(tmp_holi[1].split('-')) == 3 : #è stata indicata almeno una data
        if len(tmp_holi[1].split('h')) == 2 : # è stata indicata anche l'ora
            tmp_end_hour  = int(tmp_holi[1].split('h')[1])
            tmp_data = tmp_holi[1].split('h')[0].split('-')
            if float(tmp_data[0]) > 2000 :
                tmp_end_day   = int(tmp_data[2])
                tmp_end_month = int(tmp_data[1])
                tmp_end_year  = round(float(tmp_data[0]) - 2000)
            else :
                tmp_end_day   = int(tmp_data[0])
                tmp_end_month = int(tmp_data[1])
                tmp_end_year  = round(float(tmp_data[2]) - 2000)
        else :
            tmp_end_hour  = tmp_end_hour_default
            tmp_data = tmp_holi[1].split('h')[0].split('-')
            if float(tmp_data[0]) > 2000 :
                tmp_end_day   = int(tmp_data[2])
                tmp_end_month = int(tmp_data[1])
                tmp_end_year  = round(float(tmp_data[0]) - 2000)
            else :
                tmp_end_day   = int(tmp_data[0])
                tmp_end_month = int(tmp_data[1])
                tmp_end_year  = round(float(tmp_data[2]) - 2000)


    if round(float(tmp_holi[2])) in range(8 , 21) : # vuol dire che sto indicando la temperatura
        tmp_target_temp = round(float(tmp_holi[2]) * 2)
    else: 
        tmp_target_temp = tmp_target_temp_default

    if ( tmp_end_hour  == tmp_start_hour
            and tmp_end_day   == tmp_start_day
            and tmp_end_month == tmp_start_month
            and tmp_end_year  == tmp_start_year) :
        tmp_end_hour = tmp_end_hour_default

    if ( tmp_start_hour == NA or tmp_start_day == NA or tmp_start_month == NA or tmp_start_year == NA or tmp_end_hour == NA or tmp_end_day == NA or tmp_end_month == NA or tmp_end_year == NA or tmp_target_temp == NA ) :         
        addLog( tmp_addLog_prefix + '#505' ,  'Esiste'  , tmp_addLog_commento )
        addLog( tmp_addLog_prefix + '#507' ,  "ERRORE qualcosa è andato storto"  , tmp_addLog_commento )
        return('') # se qualcosa è andato storto
    else :
        risposta = "%4d-%02d-%02dh%02d %4d-%02d-%02dh%02d %3.1f" % (tmp_start_year +2000 , tmp_start_month, tmp_start_day, tmp_start_hour, tmp_end_year+2000, tmp_end_month, tmp_end_day, tmp_end_hour, float(tmp_target_temp / 2) )
        addLog( tmp_addLog_prefix + '#495' ,  "strToHoliday(%1s) => %1s" % (stringa,risposta)  , tmp_addLog_commento  )
        return(risposta)


#############################################################

def HolidayToBytearray(stringa) :
    return([]) # se qualcosa è andato storto



#############################################################

def HolidayToValue(stringa) :
    return('') # se qualcosa è andato storto




#############################################################

def deleteHoliday(device,slotNr) :
    tmp_deleteHoliday_Hn = "deleteHoliday() H%1d" % slotNr
    tmp_Holiday_n = "Holiday_%1d" % slotNr
    tmp_slot_holi_uuid      = "47e9ee2%1d-47e9-11e4-8939-164230d1df67" % (slotNr -1)

    NA = 0
    tmp_slot_holi_NA        = [NA,NA,NA,NA,NA,NA,NA,NA,NA]
    
    if tmp_Holiday_n in RISPOSTA['program']['HOLIDAYS'] :
        addLog( tmp_deleteHoliday_Hn + ' #183' ,  'Esiste' )
        if scriviBT( device , tmp_slot_holi_uuid , tmp_slot_holi_NA) :
            addLog( tmp_deleteHoliday_Hn + ' #185' ,  'CANCELLATO' )
            return(slotNr)
        else :
            addLog( tmp_deleteHoliday_Hn + ' #185' ,  'ERRORE in fase di cancellazione' )

    return(0)

#############################################################

def cleanHoliday(device, slotNr) :
    tmp_cleanHoliday_Hn = "cleanHoliday() H%1d" % slotNr
    tmp_Holiday_n = "Holiday_%1d" % slotNr
    tmp_slot_holi_uuid      = "47e9ee2%1d-47e9-11e4-8939-164230d1df67" % (slotNr -1)

    NA = 0
    tmp_slot_holi_NA        = [NA,NA,NA,NA,NA,NA,NA,NA,NA]
    
    if tmp_Holiday_n in RISPOSTA['program']['HOLIDAYS']:
        tmp_to = RISPOSTA['program']['HOLIDAYS'][tmp_Holiday_n]['to']
        if isinstance(tmp_to,int) :
            tmp_to = "%1d" % tmp_to
        addLog( tmp_cleanHoliday_Hn + ' #esiste' ,  'provvedo a pulire, se necessario' )
        addLog( tmp_cleanHoliday_Hn + ' per formattare', tmp_to)

        
        try:
            tmp_old_end = datetime.datetime.strptime( tmp_to, "%Y-%m-%d %H:%M:%S")
        except:
            addLog( tmp_cleanHoliday_Hn ,"problemi a riga 563. Non riesco a decifrare il formato %1s" % tmp_to)
            #print(json.dumps(RISPOSTA, sort_keys=False, indent=2))
            #sys.exit(19)
            if tmp_to[11:20] == 'finished' :
                tmp_old_end = adesso + datetime.timedelta(hours=-10)
            else :
                tmp_old_end = adesso
        
        if (tmp_old_end < adesso + datetime.timedelta(hours=-1) ) :
            
            if tmp_to[11:20] == 'finished' :
                addLog( tmp_cleanHoliday_Hn + ' #finished' ,  'possibile cancellare' )
            else :
                addLog( tmp_cleanHoliday_Hn + ' #scaduto' ,  'possibile cancellare' )

            if deleteHoliday(device, slotNr) :
                addLog( tmp_cleanHoliday_Hn + ' #deleteHoliday()' ,  'CANCELLATO' )
                return(slotNr)
            else:
                addLog( tmp_cleanHoliday_Hn + ' #deleteHoliday()' ,  'WARNING: contro ogni previsione, pare NON sia stato CANCELLATO' )

        else: 
            addLog( tmp_cleanHoliday_Hn + ' #NONscaduto' ,  'lo lascio stare' )
    return(0)

#############################################################
def get_device_name(device) :
    risposta = ''
    tmp = leggiBT( device , "00002a00-0000-1000-8000-00805f9b34fb")
    for c in tmp:
        risposta = risposta + chr(c)
    return(risposta)

#############################################################
def get_device_ID(device) :
    risposta = ''
    tmp = leggiBT( device , "00002a23-0000-1000-8000-00805f9b34fb")
    for c in tmp:
        risposta = risposta + ("%02x" % c)
    return(risposta)

#############################################################
def get_device_model(device) :
    risposta = ''
    tmp = leggiBT( device , "00002a24-0000-1000-8000-00805f9b34fb")
    for c in tmp:
        risposta = risposta + chr(c) 
    return(risposta)

#############################################################
def get_device_firmware(device) :
    risposta = ''
    tmp = leggiBT( device , "00002a26-0000-1000-8000-00805f9b34fb")
    for c in tmp:
        risposta = risposta + chr(c)
    return(risposta)

#############################################################
def get_device_software(device) :
    risposta = ''
    tmp = leggiBT( device , "00002a28-0000-1000-8000-00805f9b34fb",'get_device_software()')
    for c in tmp:
        risposta = risposta + chr(c)
    return(risposta)

#############################################################
def get_device_manufacturer(device) :
    risposta = ''
    tmp = leggiBT( device , "00002a29-0000-1000-8000-00805f9b34fb",'get_device_manufacturer()')
    for c in tmp:
        risposta = risposta + chr(c)
    return(risposta)

#############################################################
def get_device_software2(device) :
    risposta = ''
    if pinIsOK() :
        try :
            tmp = leggiBT( device , "47e9ee2d-47e9-11e4-8939-164230d1df67",'get_device_software2()')
            for c in tmp:
                risposta = risposta + chr(c)
        except :
            set_pinIsOK(False)
    else :
        addLog('get_device_software2()','ERROR: pin non immesso. Non ho diritti di lettura',pin)
    return(risposta)

#############################################################
def get_device_battery(device) :
    risposta = None
    if pinIsOK() :
        try :
            tmp = leggiBT( device , "47e9ee2c-47e9-11e4-8939-164230d1df67",'get_device_battery()')
            if tmp : risposta = tmp[0]
        except :
            set_pinIsOK(False)
    else :
        addLog('get_device_battery()','ERROR: pin non immesso. Non ho diritti di lettura',pin)
    return(risposta)

#############################################################
def get_device_datetime(device) :
    risposta = ''
    if pinIsOK() :
        try :
            tmp = leggiBT( device , "47e9ee01-47e9-11e4-8939-164230d1df67",'datetime mm:HH DD-MM-YY get_device_datetime()' )
            if tmp : risposta =  "%4d-%02d-%02d %02d:%02d:00" % (2000+tmp[4], tmp[3],tmp[2],tmp[1],tmp[0]) 
        except :
            set_pinIsOK(False)

    else :
        addLog('get_device_datetime()','ERROR: pin non immesso. Non ho diritti di lettura'  ,  pisIsOK()   )
    return(risposta)

#############################################################
def set_device_datetime(device) :
    risposta = 0
    if pinIsOK() :
        try :
            tmp_ora_esatta = datetime.datetime.now() + datetime.timedelta(seconds=30) 
            tmp_year    = int(tmp_ora_esatta.year -2000)
            tmp_month   = int(tmp_ora_esatta.month)
            tmp_day     = int(tmp_ora_esatta.day)
            tmp_hour    = int(tmp_ora_esatta.hour)
            tmp_minute  = int(tmp_ora_esatta.minute)
            datetime_as_bytearray = [  tmp_minute , tmp_hour ,  tmp_day , tmp_month , tmp_year ]
            try :
                scriviBT(device,"47e9ee01-47e9-11e4-8939-164230d1df67",datetime_as_bytearray , 'set_device_datetime()')
                risposta = 1
            except :
                addLog("set_device_datetime() ERROR","Qualcosa è andato storto in fase di scrittura scriviBT()")
        except :
            addLog("set_device_datetime() ERROR","Qualcosa è andato storto")
    else :
        addLog("set_device_datetime() ERROR","Non posso impostare l'orologio, perché PIN mancante o non valido")

    return(risposta)

#############################################################
def get_device_LCD(device) :
    risposta = {}
    if pinIsOK() :
        try :
            tmp = leggiBT( device , "47e9ee2e-47e9-11e4-8939-164230d1df67",'get_device_LCD()')
            if tmp :
                risposta['timeout'] = tmp[0]
                risposta['countdown'] = tmp[1]
        except :
            set_pinIsOK(False)
    else :
        addLog('get_device_LCD()','ERROR: pin non immesso. Non ho diritti di lettura'  ,  pinIsOK()   )
    return(risposta)

#############################################################
def get_program_settings(device) :
    risposta = None
    if pinIsOK() :
        try :
            risposta = leggiBT( device , "47e9ee2a-47e9-11e4-8939-164230d1df67","settings vari")
        except :
            risposta = None
            set_pinIsOK(False)
    return(risposta)
#############################################################
def program_settings_to_dict(trebyte) :
    risposta = {}
    if trebyte :
        # Status
        # ho notato valori diversi da 0 per
        # 0 0  1=manual 0=auto
        # 0 7  sicurezza bimbi (1=attiva, 0=disattivata)    
        # 1 3  modificato manualmente temp desiderata
        # 1 0  (ha a che fare con "party"? o window? oppure con "almeno una impostazione comfort non valida"?
        # 2 3  (forse valvola aperta? oppure distingue periodo "giorno" da "notte"?
        risposta['asByte'] = trebyte
        tmp_valvola = 0
        tmp_valvola_ordine = 0
        i = 0
        while i < 3: 
            j = 0
            while j < 8:
                if (i==0) and  (j==0):
                    tmp_nome = 'manual' #  messo in modalita manuale
                    tmp_valore = [ False,True][read_bit(trebyte[i],j)] 
                elif (i==0) and  (j==2):
                    tmp_nome = 'valvola_aprire'   
                    tmp_valore = [ False,True][read_bit(trebyte[i],j)] 
                    if tmp_valore :
                        tmp_valvola_ordine = +1
                        risposta[tmp_nome] = tmp_valore
                elif (i==0) and  (j==3):
                    tmp_nome = 'valvola_chiudere'   
                    tmp_valore = [ False,True][read_bit(trebyte[i],j)] 
                    if tmp_valore :
                        tmp_valvola_ordine = -1
                        risposta[tmp_nome] = tmp_valore
                elif (i==0) and  (j==7):
                    tmp_nome = 'lock'   # blocco bimbi attivo
                    tmp_valore = [ False,True][read_bit(trebyte[i],j)] 
                elif (i==1) and  (j==3):
                    tmp_nome = 'temp_desiderata'   # da dove viene il valore della temperatura desiderata
                    tmp_valore = [ 'preset','manual'][read_bit(trebyte[i],j)] 
                else:
                    tmp_nome = "%1d %1d" % (i,j)
                    tmp_valore = read_bit(trebyte[i],j)
                
                if (i==1):
                    if (j==0) : tmp_valvola = tmp_valvola + 1 * tmp_valore
                    if (j==1) : tmp_valvola = tmp_valvola + 2 * tmp_valore
                    if (j==2) : tmp_valvola = tmp_valvola + 4 * tmp_valore

                if tmp_valore or tmp_nome in ('manual','lock','temp_desiderata') : 
                    risposta[tmp_nome] = tmp_valore
                j = j + 1
            i = i + 1

        if tmp_valvola == 0 :
            if tmp_valvola_ordine == +1:
                risposta['valvola'] = 'closed to open'
            else :
                risposta['valvola'] = 'closed'

        elif tmp_valvola == 7 :
            if tmp_valvola_ordine == +1:
                risposta['valvola'] = 'open to close'
            else :
                risposta['valvola'] = 'open'
            risposta.pop('1 0',None)
            risposta.pop('1 1',None)
            risposta.pop('1 2',None)

        elif tmp_valvola == 1 :
            if tmp_valvola_ordine == +1:
                risposta['valvola'] = 'opening from closed'
            elif tmp_valvola_ordine == -1:
                risposta['valvola'] = 'closing from open, final adaption'
            else :
                risposta['valvola'] = 'opening'
            risposta.pop('1 0',None)

        elif tmp_valvola == 6 :
            if tmp_valvola_ordine == -1:
                risposta['valvola'] = 'closing from open'
            else :
                risposta['valvola'] = 'closing'
            risposta.pop('1 1',None)
            risposta.pop('1 2',None)

        else :
            risposta['valvola'] = 'unknow'

    else :
        addLog('get_program_settings()','ERROR: pin non immesso. Non ho diritti di lettura',   pinIsOK() )

    risposta.pop('asByte',None)
    return(risposta)
#{"manual": false, "0 1": 0, "0 2": 0, "0 3": 0, "0 4": 0, "0 5": 0, "0 6": 0, "1 0": 0, "1 1": 0, "1 2": 0, "1 3": 0, "1 4": 0, "1 5": 0, "1 6": 0, "2 0": 0, "2 1": 0, "2 2": 0, "2 3": 1, "2 4": 0, "2 5": 0, "2 6": 0}


#        RISPOSTA['program']['manual'] = [ False,True][read_bit(tmp[0],0)] # manuale
#        RISPOSTA['program']['0 1'] = [ 'off','on'][read_bit(tmp[0],1)]
#        RISPOSTA['program']['0 2'] = [ 'off','on'][read_bit(tmp[0],2)]
#        RISPOSTA['program']['0 3'] = [ 'off','on'][read_bit(tmp[0],3)]
#        RISPOSTA['program']['0 4'] = [ 'off','on'][read_bit(tmp[0],4)]
#        RISPOSTA['program']['0 5'] = [ 'off','on'][read_bit(tmp[0],5)]
#        RISPOSTA['program']['0 6'] = [ 'off','on'][read_bit(tmp[0],6)]
#        RISPOSTA['program']['lock'] = [ False,True][read_bit(tmp[0],7)] # blocco bimbi attivo
#        RISPOSTA['program']['1 0'] = [ 'off','on'][read_bit(tmp[1],0)]
#        RISPOSTA['program']['1 1'] = [ 'off','on'][read_bit(tmp[1],1)]
#        RISPOSTA['program']['1 2'] = [ 'off','on'][read_bit(tmp[1],2)]
#        RISPOSTA['program']['1 3'] = [ 'off','on'][read_bit(tmp[1],3)]
#        RISPOSTA['program']['1 4'] = [ 'off','on'][read_bit(tmp[1],4)]
#        RISPOSTA['program']['1 5'] = [ 'off','on'][read_bit(tmp[1],5)]
#        RISPOSTA['program']['1 6'] = [ 'off','on'][read_bit(tmp[1],6)]
#        RISPOSTA['program']['1 7'] = [ 'off','on'][read_bit(tmp[1],7)]
#        RISPOSTA['program']['2 0'] = [ 'off','on'][read_bit(tmp[2],0)]
#        RISPOSTA['program']['2 1'] = [ 'off','on'][read_bit(tmp[2],1)]
#        RISPOSTA['program']['2 2'] = [ 'off','on'][read_bit(tmp[2],2)]
#        RISPOSTA['program']['2 3'] = [ 'off','on'][read_bit(tmp[2],3)]
#        RISPOSTA['program']['2 4'] = [ 'off','on'][read_bit(tmp[2],4)]
#        RISPOSTA['program']['2 5'] = [ 'off','on'][read_bit(tmp[2],5)]
#        RISPOSTA['program']['2 6'] = [ 'off','on'][read_bit(tmp[2],6)]
#        RISPOSTA['program']['2 7'] = [ 'off','on'][read_bit(tmp[2],7)]





#############################################################
def get_program_temp(device) :
    risposta = {}
    if pinIsOK() :
        tmp = leggiBT( device , "47e9ee2b-47e9-11e4-8939-164230d1df67" , 'get_program_temp()')
            
        if tmp :    
            risposta['misurata']   = tmp[0] /2
            risposta['desiderata'] = tmp[1] /2
            risposta['notte']      = tmp[2] /2
            risposta['giorno']     = tmp[3] /2
            risposta['offset']     = (tmp[4] - read_bit(tmp[4],1)*256) / 2 # tmp[4] /2 # tra -5 e +5, passi di 1/2 grado
            risposta['finestra'] = {} 
            risposta['finestra']['sensibilita'] = tmp[5]
            risposta['finestra']['durata'] = tmp[6] # 10, 20, 30, 40, 50, 60
            if (tmp[5] ==  4) : risposta['finestra']['sensibilita'] = 'high'
            if (tmp[5] ==  8) : risposta['finestra']['sensibilita'] = 'mid'
            if (tmp[5] == 12) : risposta['finestra']['sensibilita'] = 'low'
    else :
        addLog('get_program_temp()','ERROR: pin non immesso. Non ho diritti di lettura', pinIsOK()  )
    return(risposta)

#############################################################
        #i = 0 # lunedi = DAYS[1]
        # ipotes codice nuovo
        # for day_i in range(0,7)
        #     tmp_slots = get_program_days(device , day_i+1 )
        #     if tmp :
        #         RISPOSTA['program']['DAYS'][DAYS[day_i + 1]] = tmp_slots
        # oppure, se restituisce un dict con il nome della settimana, utile nel caso si chieda "oggi"
        #         RISPOSTA['program']['DAYS'] = tmp_slots
        #         
def get_program_days( device, day_nr ) :        
    # NB day_nr = 1 = lunedi ... day_nr = 7 = domenica

    if isinstance(day_nr,int) :
        day_nr = [day_nr]

    risposta = {}
    NA = [None, 0]

    for myday in day_nr :
        tmp_slots = [[None,None],[None,None],[None,None],[None,None]]
        if myday > 7 : # se fuori range, allora decido che vuole oggi (=0)
            myday = 0

        if myday == 0 :  # 0 = oggi
        # mettere qui codice che stabilisce che giorno è oggi
            myday = 3


        if pinIsOK() :
            tmp_uuid = "47e9ee1%1d-47e9-11e4-8939-164230d1df67" % (myday -1)
            tmp = leggiBT( device , tmp_uuid, "get_program_Day( %1s )" % myday)

            if tmp :
                for j in [0,1,2,3,4,5,6,7]: # 0 1 2 3 4 5 6 7
                    tmp_slotNr = j // 2     # 0 0 1 1 2 2 3 3
                    tmp_slotPos = j % 2     # 0 1 0 1 0 1 0 1
                    if tmp[j] != 255 :
                        tmp_slots[tmp_slotNr][tmp_slotPos] = tmp[j] * 10
                 
        # li svuoto, cominciando da dietro 
        if tmp_slots[3][0] in NA and tmp_slots[3][1] in NA :
            del tmp_slots[3]
            if tmp_slots[2][0] in NA and tmp_slots[2][1] in NA :
                del tmp_slots[2]
                if tmp_slots[1][0] in NA and tmp_slots[1][1] in NA :
                    del tmp_slots[1]
                    if tmp_slots[0][0] in NA and tmp_slots[0][1] in NA :
                        del tmp_slots[0]

        if not(tmp_slots == []) :                #  for i in range(0,0): print("CIAO")
            for j in range(0,len(tmp_slots)) :   # dunque mi posso evitare forse "if", attenzione al return
                tmp_from_h = 0; 
                tmp_from_m = 0;
                tmp_to_h = 0; 
                tmp_to_m = 0;
                if (tmp_slots[j][0] != None) :
                    tmp_from_h = tmp_slots[j][0] // 60
                    tmp_to_m = tmp_slots[j][0] %  60
                if (tmp_slots[j][1] != None) :
                    tmp_to_h = tmp_slots[j][1] // 60
                    tmp_to_m = tmp_slots[j][1] %  60
                tmp_slots[j].append( "%02d:%02d-%02d:%02d" % (tmp_from_h, tmp_from_m, tmp_to_h, tmp_to_m) )
                if (tmp_slots[j][0] == None) and (tmp_slots[j][1] == None) :
                    tmp_slots[j] = []

                if tmp_slots[j] == [] : 
                    tmp_slots[j] = {}
                else :
                    tmp_slots[j] = {'from' : tmp_slots[j][0] , 'to' : tmp_slots[j][1] , 'interval':tmp_slots[j][2]}

        if tmp_slots :
            risposta[ DAYS[ myday ] ] = tmp_slots
        
    return(risposta)
    #RISPOSTA['program']['DAYS'][DAYS[day_i + 1]] = tmp_slots

#############################################################

def get_program_holidays( device , holi_nr ) :

    if isinstance(holi_nr,int) :
        holi_nr = [holi_nr]

    risposta = {'active' : False }

        #RISPOSTA['program']['HOLIDAYS'] = {'active':False}
        #i = 0
    if pinIsOK() :
        for myholiday in holi_nr :
            tmp_nome = "Holiday_%1d" % myholiday
            tmp_uuid = "47e9ee2%1d-47e9-11e4-8939-164230d1df67" % (myholiday - 1)
            tmp = leggiBT( device , tmp_uuid)
            if tmp and not((tmp[8]==0) and (tmp[7]==0) and (tmp[6]==0) and (tmp[5]==0) and (tmp[4]==0) and (tmp[3]==0) and (tmp[2]==0) and (tmp[1]==0) and (tmp[0]==0) ):
                risposta[tmp_nome] = {}

                if tmp[0] == 128 and tmp[4] != 128 :
                    risposta[tmp_nome]['active'] = True
                    risposta['active'] = tmp_nome
              
                # data e orario di inizio
                if (tmp[3]==0) and (tmp[2]==0) and (tmp[1]==0) and (tmp[0]==0):
                    risposta[tmp_nome]['from'] = 0 
                else:
                    if tmp[0] == 128 :
                        risposta[tmp_nome]['from'] = "%4d-%02d-%02d started" % (2000+tmp[3],tmp[2],tmp[1])
                    else :
                        risposta[tmp_nome]['from'] = "%4d-%02d-%02d %02d:00:00" % (2000+tmp[3],tmp[2],tmp[1],tmp[0])

                # data e orario di fine
                if (tmp[7]==0) and (tmp[6]==0) and (tmp[5]==0) and (tmp[4]==0):
                    risposta[tmp_nome]['to'] = 0 
                else:
                    risposta[tmp_nome]['to'] = "%4d-%02d-%02d %02d:00:00" % (2000+tmp[7],tmp[6],tmp[5],tmp[4])
                    if tmp[4] == 128 :
                        risposta[tmp_nome]['to'] = "%4d-%02d-%02d finished" % (2000+tmp[7],tmp[6],tmp[5])
                    else :
                        risposta[tmp_nome]['to'] = "%4d-%02d-%02d %02d:00:00" % (2000+tmp[7],tmp[6],tmp[5],tmp[4])

                # temperatura desiderata
                risposta[tmp_nome]['temp'] = tmp[8]/2
           
    return(risposta)

#############################################################

def pinIsOK() :
    return(RISPOSTA['device']['pin'])

#############################################################

def set_pinIsOK(booleano) :
    if booleano :
        RISPOSTA['device']['pin'] = True
    else :
        RISPOSTA['device']['pin'] = False
    return(pinIsOK())

#############################################################
def encode_pin(pin):
    #return(struct.pack('<I',int(pin)))
    pinHex = "%08x" % int(pin)
    pinBytearray = [0,0,0,0]
    pinBytearray[0] = int(pinHex[6:8],16)
    pinBytearray[1] = int(pinHex[4:6],16)
    pinBytearray[2] = int(pinHex[2:4],16)
    pinBytearray[3] = int(pinHex[0:2],16)
    return(pinBytearray)
    
#############################################################
def read_bit(byte,pos):
   # base = int(pos // 8)
    shift = int(pos % 8)
    return(byte & (1 <<shift)) >> shift
#############################################################
def help():
    print( os.path.basename(__file__),' -m <mac> -p <pin> -f <format>')

#############################################################
def connect(mac,pin):
    if (mac == ''):
        addLog("mac == ''", "senza mac non ci provo proprio a collegarmi")
        print(json.dumps(RISPOSTA, sort_keys=False, indent=4))
        sys.exit(5)
    #print("adapter.start()")

    RISPOSTA['device']['mac'] = mac

    adapter.start()

    try:
        #print("TRY adapter.connect(mac, 60)")
        device = adapter.connect(mac , 60 )
        #print("OK try")
    except:
        addLog( 'BLE ERROR' ,   "non riesco a collegarmi a MAC = %1s" % mac )
        print(json.dumps(RISPOSTA, sort_keys=False, indent=4))
        sys.exit(4)

    if (pin == ''):
        set_pinIsOK(False)
    else:
        pinBytearray = encode_pin(pin)
        try:
            scriviBT( device , "47e9ee30-47e9-11e4-8939-164230d1df67" , pinBytearray ,'PIN')
            addLog("INVIO PIN OK", "pare che l'abbia accettato")
            set_pinIsOK(True) # fino a prova contraria
        except:
            set_pinIsOK(False)  # print("non ha funzionato con il pin")
            addLog("INVIO PIN ERROR", "PIN non valido o comunque NON ha funzionato")
        
    
    # i seguenti forse non servono a niente
    # ma cosí facendo posso vedere qualcosa nel LOG
    #leggiBT( device , "00002a01-0000-1000-8000-00805f9b34fb","TEST")
    #leggiBT( device , "00002a02-0000-1000-8000-00805f9b34fb","TEST")
    #leggiBT( device , "00002a03-0000-1000-8000-00805f9b34fb","TEST")
    #leggiBT( device , "00002a04-0000-1000-8000-00805f9b34fb","TEST")
    #leggiBT( device , "00002a05-0000-1000-8000-00805f9b34fb","TEST")

    # adesso si torna seri
    RISPOSTA['device']['name']         = get_device_name(device)
    RISPOSTA['device']['ID']           = get_device_ID(device) 
    RISPOSTA['device']['model']        = get_device_model(device)
    RISPOSTA['device']['firmware']     = get_device_firmware(device)
    RISPOSTA['device']['software']     = get_device_software(device)
    RISPOSTA['device']['manufacturer'] = get_device_manufacturer(device)

    if (pinIsOK()) :
        RISPOSTA['device']['software2'] = get_device_software2(device)
        RISPOSTA['device']['battery']   = get_device_battery(device)
        RISPOSTA['device']['datetime']  = get_device_datetime(device)  # orologio interno
        RISPOSTA['device']['LCD']       = get_device_LCD(device) # LCD timeout

    if (pinIsOK()) :
        RISPOSTA['program'] = {}
        RISPOSTA['program']['SETTINGS'] = {}
        RISPOSTA['program']['TEMP'] = {}
        RISPOSTA['program']['DAYS'] = {}
        RISPOSTA['program']['HOLIDAYS'] = {}

        # temperature attuale, di riferimento, offset, finestra aperta
        RISPOSTA['program']['TEMP']     = get_program_temp(device)

        # decifrati solo alcuni: manuale, lock tastiera
        #TMP_asByte['PROGRAM_SETTINGS']         = get_program_settings(device)
        #RISPOSTA['program']['SETTINGS'] = program_settings_to_dict( TMP_asByte['PROGRAM_SETTINGS'] )
        #TMP['PROGRAM_SETTINGS']         = RISPOSTA['program']['SETTINGS'].pop('asByte',[])

      
    if (pinIsOK()) :
        TMP_asByte['PROGRAM_SETTINGS']         = get_program_settings(device)
        TMP_asByte['PROGRAM_SETTINGS_before']  = TMP_asByte['PROGRAM_SETTINGS']
        RISPOSTA['program']['SETTINGS'] = program_settings_to_dict(  TMP_asByte['PROGRAM_SETTINGS'] )

    if (pinIsOK()) :
        # programmi giornalieri
        #  1 = lunedi = DAYS[1]
        RISPOSTA['program']['DAYS'] = get_program_days(device , [1,2,3,4,5,6,7]) 
                 

    if (pinIsOK()) :
        # periodi di assenza holiday (8)
        RISPOSTA['program']['HOLIDAYS'] = get_program_holidays( device , [1,2,3,4,5,6,7,8] )


    if '' == RISPOSTA['device']['name']         : RISPOSTA['device'].pop( 'name'        , None)
    if '' == RISPOSTA['device']['ID']           : RISPOSTA['device'].pop( 'ID'          , None)
    if '' == RISPOSTA['device']['model']        : RISPOSTA['device'].pop( 'model'       , None)
    if '' == RISPOSTA['device']['firmware']     : RISPOSTA['device'].pop( 'firmware'    , None)
    if '' == RISPOSTA['device']['software']     : RISPOSTA['device'].pop( 'software'    , None)
    if '' == RISPOSTA['device']['manufacturer'] : RISPOSTA['device'].pop( 'manufacturer', None)
    if '' == RISPOSTA['device']['software2']    : RISPOSTA['device'].pop( 'software2'   , None)
    if '' == RISPOSTA['device']['battery']      : RISPOSTA['device'].pop( 'battery'     , None)
    if '' == RISPOSTA['device']['datetime']     : RISPOSTA['device'].pop( 'datetime'    , None)

    return(device)   # END connect(mac,pin)
#############################################################

def disconnect(commento):
    addLog("disconnect()",commento)
    adapter.stop()
#############################################################


def main(argv):
    #global PROGRAM_SETTINGS
    mac = ''
    pin = ''

    ### MQTT #######
    toMqtt     = False        # --mqtt
    broker     = 'openhab'    # --broker=openhab
    port       = 1883         # --port=1883
    topic_root = 'xavax'      # --topic_root=xavax
    topic      = ''           # --topic=<testo> # default: indirizzo mac

    RISPOSTA_format = 'json'
    
    RICHIESTA_onoff                   = 127
    RICHIESTA_manual                  = 127
    RICHIESTA_lock                    = 127

    RICHIESTA_lcd                     = -10
    RICHIESTA_set_pin                 =   0     # 6 cifre per immettere come nuovo PIN
    RICHIESTA_clock                   = False   # imposta l'ora esatta

    RICHIESTA_temp_target_now         = -10 # tra 8 e 28 
    RICHIESTA_temp_target_low         = -10 # tra 8 e 28 
    RICHIESTA_temp_target_high        = -10 # tra 8 e 28 
    RICHIESTA_temp_offset             = -10 # tra -5 e +5
    RICHIESTA_temp_window_sensitivity = -10 # 1=low 2=mid 3=high 
    RICHIESTA_temp_window_time        = -10 # tra 10 e 60 minuti
    
    RICHIESTA_DAY = {}
    RICHIESTA_DAY['LUN']   = ''  # 05:50-06:40 13:20-17:30
    RICHIESTA_DAY['MAR']   = ''  # 05:50-06:40 13:20-17:30
    RICHIESTA_DAY['MER']   = ''  # 05:50-06:40 13:20-17:30
    RICHIESTA_DAY['GIO']   = ''  # 05:50-06:40 13:20-17:30
    RICHIESTA_DAY['VEN']   = ''  # 05:50-06:40 13:20-17:30
    RICHIESTA_DAY['SAB']   = ''  # 05:50-06:40 13:20-17:30
    RICHIESTA_DAY['DOM']   = ''  # 05:50-06:40 13:20-17:30

    RICHIESTA_HOLI ={}
    RICHIESTA_HOLI['ALL'] = ''
    RICHIESTA_HOLI['H1'] = ''
    RICHIESTA_HOLI['H2'] = ''
    RICHIESTA_HOLI['H3'] = ''
    RICHIESTA_HOLI['H4'] = ''
    RICHIESTA_HOLI['H5'] = ''
    RICHIESTA_HOLI['H6'] = ''
    RICHIESTA_HOLI['H7'] = ''
    RICHIESTA_HOLI['H8'] = ''
    
    try:
        opts, args = getopt.getopt(argv,"hm:p:f:d:",["help","mac=","pin=",'log'
        ,"format=" ,'json','text','txt','tsv'
        ,'day='
        ,'LCD='
        ,'LOW=','HIGH=','NOW=','OFFSET='
        ,'WSENSITIVITY=','WTIME='
        ,'DAYS=','LUN=','MAR=','MER=','GIO=','VEN=','SAB=','DOM=','OGGI=','TODAY=','HEUTE=','DOMANI=','TOMORROW=','MORGEN='
        ,'HOLIDAYS=','H1=','H2=','H3=','H4=','H5=','H6=','H7=','H8='
        ,'MANUAL','AUTOMATIC','LOCK','UNLOCK'
        ,'mqtt','broker=','port=','topic_root=','topic='
        ,'ON','OFF'
        ,'PIN='
        ,'CLOCK'
        ])
    except getopt.GetoptError:
        help()
        addLog("try: opts, args = getopt.getopt()","ERRORE: getopt.GetoptError")
        print(json.dumps(RISPOSTA, sort_keys=False, indent=4))
        sys.exit(2)

    for opt, arg in opts:
        if opt in('-h','--help'):
            help()
            addLog('--help', "OK. FINE")
            print(json.dumps(RISPOSTA, sort_keys=False, indent=4))
            sys.exit()

        elif opt in ("--log") :
            RISPOSTA['addLOG'] = True

        elif opt in ("-m", "--mac"):
            if arg in ('bimbi','divano') :
                if RISPOSTA['gateway']['description'] == None :
                    RISPOSTA['gateway']['description'] = '<' + arg + '>'
                else :
                    RISPOSTA['gateway']['description'] = RISPOSTA['gateway']['description'] + ' <' + arg + '>'

                if   arg == 'bimbi'  : mac = '68:9E:19:00:BD:22'
                elif arg == 'divano' : mac = '74:DA:EA:B8:F4:A9'

            else                 : mac = arg

        elif opt in ("-p", "--pin"):
            pin = int(arg)
            pippo = RISPOSTA['gateway']['param']
            pippo = pippo.replace(str(pin),'******')
            addLog("getopt, -p --pin", "ho provato a mettere le stelline alla stringa con i parametri: "+pippo)
            RISPOSTA['gateway']['param'] = pippo

        elif opt in ('--PIN'):
            RICHIESTA_set_pin = int(arg)
            pippo = RISPOSTA['gateway']['param']
            pippo = pippo.replace(str(RICHIESTA_set_pin),'******')
            addLog("getopt, --PIN", "ho provato a mettere le stelline alla stringa con i parametri: "+pippo)
            RISPOSTA['gateway']['param'] = pippo

        elif opt in ('--CLOCK'):
            RICHIESTA_clock = True

        elif opt in ("-f", "--format"):
            if arg in ('tsv','txt','json'):
                RISPOSTA_format = arg
            else:
                RISPOSTA_format = 'json'
        elif opt in ('--text','--txt'):
            RISPOSTA_format = 'txt'
        elif opt in ('--tsv'):
            RISPOSTA_format = 'tsv'
        elif opt in ('--json'):
            RISPOSTA_format = 'json'

        elif opt in ('--ON'):
            RICHIESTA_onoff = 1
        elif opt in ('--OFF'):
            RICHIESTA_onoff = 0

        elif opt in ('--MANUAL'):
            RICHIESTA_manual = 1
        elif opt in ('--AUTOMATIC'):
            RICHIESTA_manual = 0

        elif opt in ('--LOCK'):
            RICHIESTA_lock = 1
        elif opt in ('--UNLOCK'):
            RICHIESTA_lock = 0

        elif opt in ('--LCD'):
            RICHIESTA_lcd = int(arg)


        elif opt in ('--NOW') :
            RICHIESTA_temp_target_now = float(arg)
        elif opt in ('--LOW') :
            RICHIESTA_temp_target_low = float(arg)
        elif opt in ('--HIGH') :
            RICHIESTA_temp_target_high = float(arg)
        elif opt in ('--OFFSET') :
            RICHIESTA_temp_offset = float(arg)

        elif opt in ('--WSENSITIVITY') :
            RICHIESTA_temp_window_sensitivity = round(float(arg))
        elif opt in ('--WTIME') :
            RICHIESTA_temp_window_time = round(float(arg))


        elif opt in ('--LUN') : RICHIESTA_DAY['LUN'] = arg
        elif opt in ('--MAR') : RICHIESTA_DAY['MAR'] = arg
        elif opt in ('--MER') : RICHIESTA_DAY['MER'] = arg
        elif opt in ('--GIO') : RICHIESTA_DAY['GIO'] = arg
        elif opt in ('--VEN') : RICHIESTA_DAY['VEN'] = arg
        elif opt in ('--SAB') : RICHIESTA_DAY['SAB'] = arg
        elif opt in ('--DOM') : RICHIESTA_DAY['DOM'] = arg
        elif opt in ('--DAYS') : 
            siglaGiorni = ['OGGI','LUN','MAR','MER','GIO','VEN','SAB','DOM']
            for giorno in (1,2,3,4,5,6,7) :
                RICHIESTA_DAY[ siglaGiorni[giorno] ] = arg
        elif opt in ('--OGGI','--TODAY','--HEUTE') :
            tmp_oggi_wd = int(datetime.datetime.now().strftime("%w"))
            tmp_oggi_wd = tmp_oggi_wd - 7 * ((tmp_oggi_wd - 1) // 7) # cosí 1=lun e 7=dom
            tmp_oggi_wd = DAYS_IT[tmp_oggi_wd].upper()[0:3]
            RICHIESTA_DAY[tmp_oggi_wd] = arg
        elif opt in ('--DOMANI','--TOMORROW','--MORGEN') :
            tmp_domani  = datetime.datetime.now() + datetime.timedelta(days=1) 
            tmp_domani_wd = int(tmp_domani.strftime("%w"))
            tmp_domani_wd = tmp_domani_wd - 7 * ((tmp_domani_wd - 1) // 7) # cosí 1=lun e 7=dom
            tmp_domani_wd = DAYS_IT[tmp_domani_wd].upper()[0:3]
            RICHIESTA_DAY[tmp_domani_wd] = arg


        elif opt in ('--HOLIDAYS') : RICHIESTA_HOLI['ALL'] = arg
        elif opt in ('--H1') : RICHIESTA_HOLI['H1'] = arg
        elif opt in ('--H2') : RICHIESTA_HOLI['H2'] = arg
        elif opt in ('--H3') : RICHIESTA_HOLI['H3'] = arg
        elif opt in ('--H4') : RICHIESTA_HOLI['H4'] = arg
        elif opt in ('--H5') : RICHIESTA_HOLI['H5'] = arg
        elif opt in ('--H6') : RICHIESTA_HOLI['H6'] = arg
        elif opt in ('--H7') : RICHIESTA_HOLI['H7'] = arg
        elif opt in ('--H8') : RICHIESTA_HOLI['H8'] = arg

        elif opt in ('--mqtt') : toMqtt = True 
        elif opt in ('--broker') : 
            broker = arg
            toMqtt = True
        elif opt in ('--port') : 
            port = arg
            toMqtt = True
        elif opt in ('--topic_root') : 
            topic_root = arg
            toMqtt = True
        elif opt in ('--topic') : 
            topic = arg
            toMqtt = True

        elif opt in ('--description') : 
            if RISPOSTA['gateway']['description'] == None :
                RISPOSTA['gateway']['description'] =  arg
            else :
                RISPOSTA['gateway']['description'] = RISPOSTA['gateway']['description'] + ' ' + arg 

            

    ############################################################################################################
    #
    ############################################################################################################
    if (mac == ''):
        help()
        addLog("mac == ''","Se non mi dici il mac allora cosa vuoi che faccia? prova con --mac=12:34:56:78:90:ab")
        print(json.dumps(RISPOSTA, sort_keys=False, indent=4))
        sys.exit(3)
    else:
        device = connect(mac,pin)
        #print(type(device))
        #print(type(device) == pygatt.backends.gatttool.device.GATTToolBLEDevice)


    ############################################################################################################
    #
    ############################################################################################################
    if pinIsOK() and RICHIESTA_clock :
        set_device_datetime(device)
    ############################################################################################################
    #
    ############################################################################################################
    if pinIsOK() and (RICHIESTA_set_pin > 0) :
        newpinBytearray = encode_pin(RICHIESTA_set_pin)
        try:
            tmp_rc = scriviBT( device , "47e9ee30-47e9-11e4-8939-164230d1df67" , newpinBytearray ,'PIN')
            if tmp_rc : 
                addLog("SET NEW PIN OK", "pare che abbia accettato il nuovo PIN")
            else :
                addLog("SET NEW PIN ERROR", "pare che NON abbia accettato il nuovo PIN")
        except:
            addLog("SET NEW PIN ERROR", "nuovo PIN pare non sia stato accettato, rimane quello vecchio")


    ############################################################################################################
    #
    ############################################################################################################
    if pinIsOK() and (RICHIESTA_onoff < 2) :
        if ('manual' in RISPOSTA['program']['SETTINGS']) :
            bkp = RISPOSTA['program']['SETTINGS']
            RISPOSTA['program']['SETTINGS'] = { 'before' : bkp , 'preparing' : {},  'after' : {}}

        #TMP['PROGRAM_SETTINGS'][0] = RICHIESTA_onoff + ((TMP['PROGRAM_SETTINGS'][0] >>1 )<<1 )
        # ---- -1-- 040000 apre   la valvola (nel secondo Byte si può leggere la situazione della valvola)
        # ---- 1--- 080000 chiude la valvola

        TMP_asByte['PROGRAM_SETTINGS'][0] = (2 - RICHIESTA_onoff) * 4 + (TMP_asByte['PROGRAM_SETTINGS'][0] & (0xff - 0x0c))
        RISPOSTA['program']['SETTINGS']['preparing'] = program_settings_to_dict(TMP_asByte['PROGRAM_SETTINGS'])

    ############################################################################################################
    if pinIsOK() and (RICHIESTA_manual < 2) :
        if ('manual' in RISPOSTA['program']['SETTINGS']) :
            bkp = RISPOSTA['program']['SETTINGS']
            RISPOSTA['program']['SETTINGS'] = { 'before' : bkp , 'preparing' : {},  'after' : {}}

        #TMP['PROGRAM_SETTINGS'][0] = RICHIESTA_manual #(RICHIESTA_manual <<7) + ((TMP['PROGRAM_SETTINGS'][0] <<1 )>>1 )
#        TMP['PROGRAM_SETTINGS'][0] = RICHIESTA_manual + ((TMP['PROGRAM_SETTINGS'][0] >>1 )<<1 )
        TMP_asByte['PROGRAM_SETTINGS'][0] = RICHIESTA_manual + (TMP_asByte['PROGRAM_SETTINGS'][0] & (0xff - 0x01))
        RISPOSTA['program']['SETTINGS']['preparing'] = program_settings_to_dict(TMP_asByte['PROGRAM_SETTINGS'])


    ############################################################################################################
    if pinIsOK() and (RICHIESTA_lock < 2) :
        if ('manual' in RISPOSTA['program']['SETTINGS']) :
            bkp = RISPOSTA['program']['SETTINGS']
            RISPOSTA['program']['SETTINGS'] = { 'before' : bkp , 'preparing' : {},  'after' : {}}

        #TMP['PROGRAM_SETTINGS'][0] = RICHIESTA_manual #(RICHIESTA_manual <<7) + ((TMP['PROGRAM_SETTINGS'][0] <<1 )>>1 )
#        TMP['PROGRAM_SETTINGS'][0] = RICHIESTA_lock<<7 + (TMP['PROGRAM_SETTINGS'][0] % 128  )
        TMP_asByte['PROGRAM_SETTINGS'][0] = RICHIESTA_lock * 128 + (TMP_asByte['PROGRAM_SETTINGS'][0] & (0xff - 0x80))
        RISPOSTA['program']['SETTINGS']['preparing'] = program_settings_to_dict(TMP_asByte['PROGRAM_SETTINGS'])

    ################################
    if (pinIsOK() and (RICHIESTA_manual <2 
        or RICHIESTA_lock <2 
        or RICHIESTA_onoff <2
        or TMP_asByte['PROGRAM_SETTINGS_before'][0] != TMP_asByte['PROGRAM_SETTINGS'][0]
        or TMP_asByte['PROGRAM_SETTINGS_before'][1] != TMP_asByte['PROGRAM_SETTINGS'][1]
        or TMP_asByte['PROGRAM_SETTINGS_before'][2] != TMP_asByte['PROGRAM_SETTINGS'][2] ) ) :

        if ('manual' in RISPOSTA['program']['SETTINGS']) :
            bkp = RISPOSTA['program']['SETTINGS']
            RISPOSTA['program']['SETTINGS'] = { 'before' : bkp , 'preparing' : {},  'after' : {}}

        RISPOSTA['program']['SETTINGS']['preparing'] = program_settings_to_dict(TMP_asByte['PROGRAM_SETTINGS'] )

        if ('before' in RISPOSTA['program']['SETTINGS']) :
            if ((RISPOSTA['program']['SETTINGS']['before']['valvola'] == 'open' )
                and ('valvola_chiudere' in RISPOSTA['program']['SETTINGS']['preparing']) 
                and (RISPOSTA['program']['SETTINGS']['preparing']['valvola'] == 'open') ):
                RISPOSTA['program']['SETTINGS']['preparing']['valvola'] = 'open to close'

            if ((RISPOSTA['program']['SETTINGS']['before']['valvola'] == 'closed') and
                ('valvola_aprire' in RISPOSTA['program']['SETTINGS']['preparing']) and
                (RISPOSTA['program']['SETTINGS']['preparing']['valvola'] == 'closed') ):
                RISPOSTA['program']['SETTINGS']['preparing']['valvola'] = 'closed to open'

    else :
        addLog("PROGRAM_SETTINGS","Non sono stati modificati, dunque niente scrittura tramite BLE",'lock e/o manual e/o onoff')

    ################################
    if pinIsOK() and (RICHIESTA_lcd >0) :
        scriviBT( device , "47e9ee2e-47e9-11e4-8939-164230d1df67" , [RICHIESTA_lcd,0] )
        tmp_lcd = RISPOSTA['device']['LCD']['timeout']
        RISPOSTA['device']['LCD']['timeout'] = {}
        RISPOSTA['device']['LCD']['timeout']['before'] = tmp_lcd
        RISPOSTA['device']['LCD']['timeout']['request'] = RICHIESTA_lcd
        tmp = get_device_LCD( device )
        if tmp : RISPOSTA['device']['LCD']['timeout']['after'] = tmp['timeout']

    ################################
    if pinIsOK() and ((RICHIESTA_temp_target_now > -10) or (RICHIESTA_temp_target_low > -10) or (RICHIESTA_temp_target_high > -10) or (RICHIESTA_temp_offset > -10) or (RICHIESTA_temp_window_sensitivity > -10) or ( RICHIESTA_temp_window_time > -10) ):
        NA                = 128
        byte_NOW          = NA
        byte_LOW          = NA
        byte_HIGH         = NA
        byte_OFFSET       = NA
        byte_WSENSITIVITY = NA
        byte_WTIME        = NA

        if RICHIESTA_temp_target_low > -10:
            #print("RICHIESTA_temp_target_low = %1d" % RICHIESTA_temp_target_low)
            tmp_temp_low = RISPOSTA['program']['TEMP']['notte']
            RISPOSTA['program']['TEMP']['notte'] = {}
            RISPOSTA['program']['TEMP']['notte']['before'] = tmp_temp_low
            RISPOSTA['program']['TEMP']['notte']['request'] = RICHIESTA_temp_target_low
            if (RICHIESTA_temp_target_low > 28):
                RISPOSTA['program']['TEMP']['notte']['error'] = "Temperatura richiesta > 28"
                RISPOSTA['program']['TEMP']['notte']['after'] = RISPOSTA['program']['TEMP']['notte']['before']
            elif (RICHIESTA_temp_target_low < 8):
                RISPOSTA['program']['TEMP']['notte']['error'] = "Temperatura richiesta < 8"
                RISPOSTA['program']['TEMP']['notte']['after'] = RISPOSTA['program']['TEMP']['notte']['before']
            else:
                byte_LOW = int(round(RICHIESTA_temp_target_low * 2))
                

        if RICHIESTA_temp_target_high > -10:
            #print("RICHIESTA_temp_target_high = %1d" % RICHIESTA_temp_target_high)
            tmp_temp_high = RISPOSTA['program']['TEMP']['giorno']
            RISPOSTA['program']['TEMP']['giorno'] = {}
            RISPOSTA['program']['TEMP']['giorno']['before'] = tmp_temp_high
            RISPOSTA['program']['TEMP']['giorno']['request'] = RICHIESTA_temp_target_high
            if (RICHIESTA_temp_target_high > 28):
                RISPOSTA['program']['TEMP']['giorno']['error'] = "Temperatura richiesta > 28"
                RISPOSTA['program']['TEMP']['giorno']['after'] = RISPOSTA['program']['TEMP']['giorno']['before']
            elif (RICHIESTA_temp_target_high < 8):
                RISPOSTA['program']['TEMP']['giorno']['error'] = "Temperatura richiesta < 8"
                RISPOSTA['program']['TEMP']['giorno']['after'] = RISPOSTA['program']['TEMP']['giorno']['before']
            else:
                byte_HIGH = int(round(RICHIESTA_temp_target_high * 2))

        if RICHIESTA_temp_offset > -10:
            tmp_temp_high = RISPOSTA['program']['TEMP']['offset']
            RISPOSTA['program']['TEMP']['offset'] = {}
            RISPOSTA['program']['TEMP']['offset']['before'] = tmp_temp_high
            RISPOSTA['program']['TEMP']['offset']['request'] = RICHIESTA_temp_offset
            if (RICHIESTA_temp_offset > 5):
                RISPOSTA['program']['TEMP']['offset']['error'] = "Temperatura richiesta > +5"
                RISPOSTA['program']['TEMP']['offset']['after'] = RISPOSTA['program']['TEMP']['offset']['before']
            elif (RICHIESTA_temp_offset < -5):
                RISPOSTA['program']['TEMP']['offset']['error'] = "Temperatura richiesta < -5"
                RISPOSTA['program']['TEMP']['offset']['after'] = RISPOSTA['program']['TEMP']['offset']['before']
            else:
                byte_OFFSET = round(RICHIESTA_temp_offset * 2)

        if RICHIESTA_temp_target_now == -10 and ((byte_HIGH != NA) or (byte_LOW != NA)) or (byte_WSENSITIVITY != NA) or (byte_WTIME != NA): #and (RISPOSTA['program']['SETTINGS']['temp_desiderata'] == 'preset') : 
            RICHIESTA_temp_target_now = -9
        if RICHIESTA_temp_target_now > -10:
            tmp_temp_now = RISPOSTA['program']['TEMP']['desiderata']
            RISPOSTA['program']['TEMP']['desiderata'] = {}
            RISPOSTA['program']['TEMP']['desiderata']['before'] = tmp_temp_now
            if (RICHIESTA_temp_target_now > 0):
                RISPOSTA['program']['TEMP']['desiderata']['request'] = RICHIESTA_temp_target_now
                byte_NOW = int(round(RICHIESTA_temp_target_now * 2))

        if RICHIESTA_temp_window_sensitivity in (1,2,3) :
            #metto il valore precedente da parte
            tmp_temp_wsens = RISPOSTA['program']['TEMP']['finestra']['sensibilita']
            RISPOSTA['program']['TEMP']['finestra']['sensibilita'] = {}
            RISPOSTA['program']['TEMP']['finestra']['sensibilita']['before'] = tmp_temp_wsens
            #1 = low  = 12 = 00001100 = 3 << 2 = (4- 1) << 2
            #2 = mid  =  8 = 00001000 = 2 << 2 = (4- 2) << 2 
            #3 = high =  4 = 00000100 = 1 << 2 = (4- 3) << 2
            if (RICHIESTA_temp_window_sensitivity == 1) :
                byte_WSENSITIVITY = 12
            elif (RICHIESTA_temp_window_sensitivity == 2) :
                byte_WSENSITIVITY = 8
            elif (RICHIESTA_temp_window_sensitivity == 3) :
                byte_WSENSITIVITY = 4

        if RICHIESTA_temp_window_time in range(5,100) :
            #metto il valore precedente da parte
            tmp_temp_wsens = RISPOSTA['program']['TEMP']['finestra']['durata']
            RISPOSTA['program']['TEMP']['finestra']['durata'] = {}
            RISPOSTA['program']['TEMP']['finestra']['durata']['before'] = tmp_temp_wsens
            byte_WTIME = RICHIESTA_temp_window_time


        
        if (byte_NOW != NA) or (byte_LOW != NA) or (byte_HIGH != NA) or (byte_OFFSET != NA) or (byte_WSENSITIVITY != NA) or (byte_WTIME != NA):

            if scriviBT( device , "47e9ee2b-47e9-11e4-8939-164230d1df67", [NA, byte_NOW, byte_LOW, byte_HIGH, byte_OFFSET, byte_WSENSITIVITY, byte_WTIME]) :

                tmp = get_program_temp( device )
                if tmp :
                    if RICHIESTA_temp_target_now         > -10: RISPOSTA['program']['TEMP']['desiderata']             ['after'] = tmp['desiderata']
                    if RICHIESTA_temp_target_low         > -10: RISPOSTA['program']['TEMP']['notte']                  ['after'] = tmp['notte']
                    if RICHIESTA_temp_target_high        > -10: RISPOSTA['program']['TEMP']['giorno']                 ['after'] = tmp['giorno']
                    if RICHIESTA_temp_offset             > -10: RISPOSTA['program']['TEMP']['offset']                 ['after'] = tmp['offset']
                    if RICHIESTA_temp_window_time        > -10: RISPOSTA['program']['TEMP']['finestra']['durata']     ['after'] = tmp['finestra']['durata']
                    if RICHIESTA_temp_window_sensitivity > -10: RISPOSTA['program']['TEMP']['finestra']['sensibilita']['after'] = tmp['finestra']['sensibilita']
            
    #####################################
    tmp_day_nr = 0
    NA = 255
    siglaGiorni = ["LUN", "MAR", "MER", "GIO", "VEN", "SAB", "DOM"]
    for giorno in siglaGiorni :
        if (RICHIESTA_DAY[giorno] != ''):
            
            tmp_day_time = [NA,NA , NA,NA , NA,NA , NA,NA] # sono 4 slots, ciascuno con un inizio e una fine
            tmp_slot_nr = 0

            if (RICHIESTA_DAY[giorno] == 'SCOLASTICO') : 
                RICHIESTA_DAY[giorno] = scolastico(siglaGiorni.index(giorno) + 1)

            if (RICHIESTA_DAY[giorno] != 'DEL') :
                tmp_slots = RICHIESTA_DAY[giorno].split()  # viene usato il blank per separare i singoli slot
                
                for t_s in tmp_slots:
                    
                    if tmp_slot_nr < 4:
                        t_d = t_s.split('-')

                        tmp_from = t_d[0].split(':')
                        tmp_day_time[ (tmp_slot_nr * 2) +0] = int(tmp_from[0])*6 + round(int(tmp_from[1])/10 )

                        tmp_to   = t_d[1].split(':')
                        tmp_day_time[ (tmp_slot_nr * 2) +1] = int(tmp_to[0])*6 + round(int(tmp_to[1])/10 )
                        
                        tmp_slot_nr = tmp_slot_nr + 1

                for i in [2,3,4] :
                    if tmp_slot_nr < i: 
                        tmp_day_time[ (i-1) *2 + 0] = NA
                        tmp_day_time[ (i-1) *2 + 1] = NA

            if (RICHIESTA_DAY[giorno] == 'DEL') or (tmp_slot_nr > 0) :
                tmp_uuid = "47e9ee1%1d-47e9-11e4-8939-164230d1df67" % tmp_day_nr
                scriviBT( device , tmp_uuid , tmp_day_time)
            
        tmp_day_nr = tmp_day_nr +1

    ######################################
    # sono 9 Byte (da 0 a 8)
    #  0 = inizio ora
    #  1 = inizio giorno
    #  2 = inizio mese
    #  3 = inizio anno (post 2000)
    #  4 = fine ora
    #  5 = fine giorno
    #  6 = fine mese
    #  7 = fine anno (post 2000)
    #  8 = temperatura di riferimento
    # uuid = 47e9ee2x-47e9-11e4-8939-164230d1df67 con x da 0 a 7 (perché ci sono 8 slot)
    

    NA = 0
    if (RICHIESTA_HOLI['ALL'] == 'DEL') :
        addLog( 'H1..H8' + ' #691 --HOLIDAYS=DEL' ,  'Richiesta di cancellazione tutti H1..H8. Se esistono te lo dico' )
        # tmp_slot_holi_NA        = [NA,NA,NA,NA,NA,NA,NA,NA,NA]
        # scrivere codice che cancella tutti gli HOLI da H1 a H8
        for tmp_slot_holi in (1,2,3,4,5,6,7,8) :
            deleteHoliday(device , tmp_slot_holi)
                
    elif (RICHIESTA_HOLI['ALL'] == 'CLEAN') :
        addLog( 'H1..H8' + ' #691 --HOLIDAYS=CLEAN' ,  'Richiesta di cancellazione tutti H1..H8 scaduti. Se esistono te lo dico' )
        # tmp_slot_holi_NA        = [NA,NA,NA,NA,NA,NA,NA,NA,NA]
        # scrivere codice che cancella tutti gli HOLI da H1 a H8
        for tmp_slot_holi in (1,2,3,4,5,6,7,8) :
            cleanHoliday(device , tmp_slot_holi)
            # sposta gli Hx eliminando quellli vuoti e mettendoli in ordine cronologico

    elif (RICHIESTA_HOLI['ALL'] != '') :
        tmp_addLog_prefix = "--HOLIDAYS=%1s" % RICHIESTA_HOLI['ALL']
        # dovrebbe essere in formato holiday e viene aggiunto a quelli esistenti
        # se non ci sono slot sufficienti allora viene eliminato quello
        # più lontano nel futuro, anche se è più vicino di quello che si aggiunge 
        # possibile usare anche la sintassi presenti in OpenStreetMap
        tmp_strToHoliday = strToHoliday( RICHIESTA_HOLI['ALL'] )
        if  tmp_strToHoliday : # quando è un formato Holi, allora restituisce qualcosa
            addLog(tmp_addLog_prefix , "pare che sia in formato Holiday" , tmp_strToHoliday)
            # dovrei vedere quali slot liberi ci sono
            # se non ci sono liberi provare anzitutto con un CLEAN e poi cercare nuovamente
            tmp_free_slots = getFreeHolidaySlots() 
            if not(tmp_free_slots) :
                addLog(tmp_addLog_prefix,"NON ci sono slot liberi, provo con un CLEAN", tmp_strToHoliday )
                for tmp_slot_holi in (1,2,3,4,5,6,7,8) :
                    cleanHoliday(device , tmp_slot_holi)

            tmp_free_slots = getFreeHolidaySlots() 
            if not(tmp_free_slots) :
                #. a questo punto necessario individuare il più lontano nel tempo
                #. ed eliminarlo
                addLog(tmp_addLog_prefix,"malgrado CLEAN NON ci sono ancora slot liberi, dovrei eliminarne uno, quello più lontano", tmp_strToHoliday )

            #tmp_free_slots = getFreeHolidaySlots() 
            if tmp_free_slots :
                addLog(tmp_addLog_prefix,"HO trovato degli slot liberi", tmp_free_slots )
                tmp_inserito = False
                for tmp_slot_holi in tmp_free_slots :
                    tmp_Hn = "H%1d" % tmp_slot_holi
                    if not(tmp_inserito) and (RICHIESTA_HOLI[tmp_Hn] == '') :
                        RICHIESTA_HOLI[tmp_Hn] = tmp_strToHoliday
                        addLog(tmp_addLog_prefix , "Uso lo slot %1d che non risulta prenotato" % tmp_slot_holi , tmp_strToHoliday )
                        tmp_inserito = True
                    elif not(tmp_inserito) and (RICHIESTA_HOLI[tmp_Hn] != '') :
                        addLog(tmp_addLog_prefix , "Lo slot %1d che risulta PRENOTATO" % tmp_slot_holi , tmp_strToHoliday )

                if not(tmp_inserito) :
                    addLog(tmp_addLog_prefix , "ERRORE: tutti gli slot liberi o liberati risultano PRENOTATI", tmp_strToHoliday )
            else :
                addLog(tmp_addLog_prefix , "ERRORE: NON risultano slot liberi o liberati", tmp_strToHoliday )
                addLog(tmp_addLog_prefix , "ERRORE: devo IGNORARE la richiesta", tmp_strToHoliday )
        else : 
            addLog(tmp_addLog_prefix , "NON so come interpretarlo","")
    #else :





    #####################################
    adesso = datetime.datetime.now()  + datetime.timedelta(hours=1) 
    tmp_slot_holi_NA        = [NA,NA,NA,NA,NA,NA,NA,NA,NA]
    tmp_start_hour_default = 7
    tmp_end_hour_default   = 23
    #print(RISPOSTA)
    #print(json.dumps(RISPOSTA, sort_keys=False, indent=4))
    if pinIsOK(): 
        if isinstance(RISPOSTA['program']['TEMP']['notte'], dict) :
            tmp_target_temp_default = round(RISPOSTA['program']['TEMP']['notte']['after'] * 2)
        else: 
            tmp_target_temp_default = round(RISPOSTA['program']['TEMP']['notte'] * 2)

    #tmp_slot_holi           = 3     #(conto da 1 a 8)
    
    for tmp_slot_holi in (1,2,3,4,5,6,7,8) :
        tmp_slot_holi_uuid      = "47e9ee2%1d-47e9-11e4-8939-164230d1df67" % (tmp_slot_holi -1)
        tmp_Holiday_n = "Holiday_%1d" % tmp_slot_holi
        tmp_Hn = "H%1d" % tmp_slot_holi

        if (RICHIESTA_HOLI[tmp_Hn] != '') :
            addLog( tmp_Hn + ' #760' ,  'Richiesta di impostare/cancellare' )

            tmp_slot_holi_value     = tmp_slot_holi_NA
            tmp_start_hour  = NA # 0
            tmp_start_day   = NA # 1
            tmp_start_month = NA # 2
            tmp_start_year  = NA # 3
            tmp_end_hour  = NA   # 4
            tmp_end_day   = NA   # 5
            tmp_end_month = NA   # 6 
            tmp_end_year  = NA   # 7 
            tmp_target_temp = NA # 8

            
            addLog( tmp_Hn + ' #option (tmp_slot_holi)' ,  tmp_slot_holi  )
            addLog( tmp_Hn + ' #tmp_Holiday_n' ,  tmp_Holiday_n )
            addLog( tmp_Hn + ' #tmp_Hn' ,  tmp_Hn )
            addLog( tmp_Hn + ' #parametro' ,  RICHIESTA_HOLI[tmp_Hn] )
            addLog( tmp_Hn + ' #tmp_slot_holi_uuid' ,  tmp_slot_holi_uuid  )


            if (RICHIESTA_HOLI[tmp_Hn] == 'DEL') :

                addLog( tmp_Hn + ' #DEL' ,  'Richiesta di cancellare' )
                if deleteHoliday(device , tmp_slot_holi) :
                    addLog( tmp_Hn + ' #deleteHoliday()' ,  'CANCELLATO' )
                else:
                    addLog( tmp_Hn + ' #deleteHoliday()' ,  'WARNING: contro ogni previsione, pare NON sia stato CANCELLATO' )

            elif (RICHIESTA_HOLI[tmp_Hn] == 'CLEAN') :
            
                addLog( tmp_Hn + ' #CLEAN' ,  'Richiesta di cancellare se scaduto' )

                if cleanHoliday(device , tmp_slot_holi) :
                    addLog( tmp_Hn + ' #CLEAN' ,  'Era scaduto ed è stato cancellato' )
                

            else : #########################################################################
                addLog( tmp_Hn + ' #716' ,  "1 la richiesta è '%1s'" % RICHIESTA_HOLI[tmp_Hn]  )


                tmp_holi =strToHoliday(RICHIESTA_HOLI[tmp_Hn]).split() 

                if len(tmp_holi)==1 : # vuol dire che ho indicato soltanto il giorno
                    tmp_holi = [tmp_holi[0] , tmp_holi[0], "%3.1f" % (tmp_target_temp_default / 2)]
                    addLog( tmp_Hn + ' #720' ,  'ERR 2 è stato indicato soltanto un giorno (di avvio e dunque di arresto)' )
                    addLog( tmp_Hn + ' #722' ,  "ERR 2 la richiesta adesso è '%1s %1s %1s'" % (tmp_holi[0],tmp_holi[1],tmp_holi[2])  )

                if len(tmp_holi)==2 and ((tmp_holi[1] in ('DOMANI','TOMORROW',tmp_holi[0])) or (len(tmp_holi[1].split('-'))>2)) :
                    tmp_holi = [tmp_holi[0] , tmp_holi[1], "%3.1f" % (tmp_target_temp_default / 2)]
                    addLog( tmp_Hn + ' #726' ,  'ERR 3 stato indicato soltanto INIZIO e FINE, la temperatura ce la metto io' )

                elif len(tmp_holi)==2 and not((tmp_holi[1] in ('DOMANI','TOMORROW',tmp_holi[0])) or (len(tmp_holi[1].split('-'))>2)):
                    tmp_holi = [tmp_holi[0] , tmp_holi[0], tmp_holi[1]] 
                    addLog( tmp_Hn + ' #729' ,  'ERR 4 stato indicato soltanto un giorno (di avvio e dunque di arresto) e la temperatura' )


                addLog( tmp_Hn + ' #731' ,  "5 la richiesta adesso è '%1s %1s %1s'" % (tmp_holi[0],tmp_holi[1],tmp_holi[2])  )

                if tmp_holi[1] in ('NOW','ADESSO'):
                    tmp_holi[1] = 'OGGI'
                    addLog( tmp_Hn + ' #735' ,  'ERR 6 modifico il secondo paramtero da NOW/ADESSO a OGGI/TODAY' )

          
                addLog( tmp_Hn + ' #738' ,  "7 la richiesta adesso è '%1s %1s %1s'" % (tmp_holi[0],tmp_holi[1],tmp_holi[2])  )

                if tmp_holi[0] in ('NOW','ADESSO') :
                    tmp_start_hour  = adesso.hour
                    tmp_start_day   = adesso.day
                    tmp_start_month = adesso.month
                    tmp_start_year  = adesso.year - 2000
                    addLog( tmp_Hn + ' #1068' ,  'ERR 0 NOW? ADESSO?'  )
                elif tmp_holi[0] in ('OGGI','TODAY','HEUTE') :
                    tmp_start_hour  = max( adesso.hour , tmp_start_hour_default )
                    tmp_start_day   = adesso.day
                    tmp_start_month = adesso.month
                    tmp_start_year  = adesso.year - 2000
                    addLog( tmp_Hn + ' #1074' ,  'ERR 0 OGGI? TODAY?'  )
                elif tmp_holi[0] in ('DOMANI','TOMORROW') :
                    domani = adesso + datetime.timedelta(days=1) 
                    tmp_start_hour  = tmp_start_hour_default     # comincia alle sette di mattino
                    tmp_start_day   = domani.day
                    tmp_start_month = domani.month
                    tmp_start_year  = domani.year - 2000
                    addLog( tmp_Hn + ' #1081' ,  'ERR 0 DOMANI? TOMORROW?'  )
                elif len(tmp_holi[0].split('-')) == 3 : #è stata indicata almeno una data
                    if len(tmp_holi[0].split('h')) == 2 : # è stata indicata anche l'ora
                        tmp_start_hour  = int(tmp_holi[0].split('h')[1])
                        tmp_data = tmp_holi[0].split('h')[0].split('-')
                        if float(tmp_data[0]) > 2000 :
                            tmp_start_day   = int(tmp_data[2])
                            tmp_start_month = int(tmp_data[1])
                            tmp_start_year  = round(float(tmp_data[0]) - 2000)
                        else :
                            addLog( tmp_Hn + ' #1091' ,  'ERR 0 anno al terzo posto?'  )
                            tmp_start_day   = int(tmp_data[0])
                            tmp_start_month = int(tmp_data[1])
                            tmp_start_year  = round(float(tmp_data[2]) - 2000)
                    else :
                        addLog( tmp_Hn + ' #1095' ,  'ERR 0 manca ora di avvio?'  )
                        tmp_start_hour  = tmp_start_hour_default
                        tmp_data = tmp_holi[0].split('h')[0].split('-')
                        if float(tmp_data[0]) > 2000 :
                            tmp_start_day   = int(tmp_data[2])
                            tmp_start_month = int(tmp_data[1])
                            tmp_start_year  = round(float(tmp_data[0]) - 2000)
                        else :
                            addLog( tmp_Hn + ' #1103' ,  'ERR 0 anno al terzo posto?'  )
                            tmp_start_day   = int(tmp_data[0])
                            tmp_start_month = int(tmp_data[1])
                            tmp_start_year  = round(float(tmp_data[2]) - 2000)
                   

                if (tmp_holi[1] in ('DOMANI','TOMORROW')) :
                    addLog( tmp_Hn + ' #1110' ,  'ERR 1 DOMANI? TOMORROW?'  )
                    domani = adesso + datetime.timedelta(days=1) 
                    tmp_end_hour  = tmp_end_hour_default
                    tmp_end_day   = domani.day
                    tmp_end_month = domani.month
                    tmp_end_year  = domani.year - 2000
                elif tmp_holi[1] in ('OGGI','TODAY','HEUTE') :
                    addLog( tmp_Hn + ' #1120' ,  'ERR 1 OGGI? TODAY?'  )
                    tmp_end_hour  = tmp_end_hour_default 
                    tmp_end_day   = adesso.day
                    tmp_end_month = adesso.month
                    tmp_end_year  = adesso.year - 2000
                #elif tmp_holi[1] in ('NOW','ADESSO') :
                #    tmp_start_hour  = adesso.hour
                #    tmp_start_day   = adesso.day
                #    tmp_start_month = adesso.month
                #    tmp_start_year  = adesso.year - 2000
                elif len(tmp_holi[1].split('-')) == 3 : #è stata indicata almeno una data
                    if len(tmp_holi[1].split('h')) == 2 : # è stata indicata anche l'ora
                        tmp_end_hour  = int(tmp_holi[1].split('h')[1])
                        tmp_data = tmp_holi[1].split('h')[0].split('-')
                        if float(tmp_data[0]) > 2000 :
                            tmp_end_day   = int(tmp_data[2])
                            tmp_end_month = int(tmp_data[1])
                            tmp_end_year  = round(float(tmp_data[0]) - 2000)
                        else :
                            addLog( tmp_Hn + ' #1137' ,  'ERR 1 anno al terzo posto?'  )
                            tmp_end_day   = int(tmp_data[0])
                            tmp_end_month = int(tmp_data[1])
                            tmp_end_year  = round(float(tmp_data[2]) - 2000)
                    else :
                        addLog( tmp_Hn + ' #1145' ,  'ERR 1 manca ora di stop?'  )
                        tmp_end_hour  = tmp_end_hour_default
                        tmp_data = tmp_holi[1].split('h')[0].split('-')
                        if float(tmp_data[0]) > 2000 :
                            tmp_end_day   = int(tmp_data[2])
                            tmp_end_month = int(tmp_data[1])
                            tmp_end_year  = round(float(tmp_data[0]) - 2000)
                        else :
                            addLog( tmp_Hn + ' #1151' ,  'ERR 1 anno al terzo posto?'  )
                            tmp_end_day   = int(tmp_data[0])
                            tmp_end_month = int(tmp_data[1])
                            tmp_end_year  = round(float(tmp_data[2]) - 2000)


                if round(float(tmp_holi[2])) in range(8 , 21) : # vuol dire che sto indicando la temperatura
                    tmp_target_temp = round(float(tmp_holi[2]) * 2)
                else: 
                    tmp_target_temp = tmp_target_temp_default

                if ( tmp_end_hour  == tmp_start_hour
                        and tmp_end_day   == tmp_start_day
                        and tmp_end_month == tmp_start_month
                        and tmp_end_year  == tmp_start_year) :
                    tmp_end_hour = tmp_end_hour_default



            tmp_start_hour = int(tmp_start_hour)
            tmp_start_day = int(tmp_start_day)
            tmp_start_month = int(tmp_start_month)
            tmp_start_year = int(tmp_start_year)
            tmp_end_hour = int(tmp_end_hour)
            tmp_end_day = int(tmp_end_day)
            tmp_end_month = int(tmp_end_month)
            tmp_end_year = int(tmp_end_year)
            tmp_target_temp = int(tmp_target_temp)

            tmp_slot_holi_value = [ tmp_start_hour
                                  , tmp_start_day
                                  , tmp_start_month
                                  , tmp_start_year
                                  , tmp_end_hour
                                  , tmp_end_day
                                  , tmp_end_month
                                  , tmp_end_year
                                  , tmp_target_temp]

            addLog( tmp_Hn + ' #tmp_start_hour'  ,  tmp_start_hour )
            addLog( tmp_Hn + ' #tmp_start_day'   ,  tmp_start_day )
            addLog( tmp_Hn + ' #tmp_start_month' ,  tmp_start_month )
            addLog( tmp_Hn + ' #tmp_start_year'  ,  tmp_start_year )
            addLog( tmp_Hn + ' #tmp_end_hour'    ,  tmp_end_hour )
            addLog( tmp_Hn + ' #tmp_end_day'     ,  tmp_end_day )
            addLog( tmp_Hn + ' #tmp_end_month'   ,  tmp_end_month )
            addLog( tmp_Hn + ' #tmp_end_year'    ,  tmp_end_year )
            addLog( tmp_Hn + ' #tmp_target_temp' ,  tmp_target_temp )


            if (tmp_start_hour >= 0 and tmp_start_day> 0 and tmp_start_month > 0 and tmp_start_year > 0 
            and tmp_start_hour < 24 and tmp_start_day<32 and tmp_start_month <13 and tmp_start_year < adesso.year+2-2000 
            and tmp_end_hour   >= 0 and tmp_end_day  > 0 and tmp_end_month   > 0 and tmp_end_year   > 0 
            and tmp_end_hour   < 24 and tmp_end_day  <32 and tmp_end_month   <13 and tmp_end_year   < adesso.year+2-2000 and tmp_end_year >= tmp_start_year
            and tmp_target_temp>7*2 
            and tmp_target_temp<21*2 ) :
                if (datetime.datetime(tmp_end_year, tmp_end_month, tmp_end_day, tmp_end_hour) 
                  > datetime.datetime(tmp_start_year, tmp_start_month, tmp_start_day, tmp_start_hour)):                    

                    scriviBT( device , tmp_slot_holi_uuid ,tmp_slot_holi_value )
                    addLog( tmp_Hn + ' #761' ,  'forse andato a buon fine scrittura di Holiday_N' )

            else :
                addLog( tmp_Hn + ' #764' ,  'qualche valore per Holiday_N è non valido' )

    ################################################################
    #                                                              #
    ################################################################
    
    richieste_days = 0
    for k in RICHIESTA_DAY :
        if RICHIESTA_DAY[k] : richieste_days = richieste_days + 1

    if richieste_days > 0 :
        bkp = RISPOSTA['program']['DAYS']
        RISPOSTA['program']['DAYS'] = {
            'before' : bkp
            ,'after' : get_program_days(device , [1,2,3,4,5,6,7])
        }
        ##### aggiungere codice che metta in evidenza in RISPOSTA
        ##### le differenze tra prima e dopo (almeno i campi) 
                 


    ################################################################
    richieste_holidays = 0
    for k in RICHIESTA_HOLI :
        if RICHIESTA_HOLI[k] : richieste_holidays = richieste_holidays + 1

    if richieste_holidays > 0 :
        bkp = RISPOSTA['program']['HOLIDAYS']
        RISPOSTA['program']['HOLIDAYS'] = { 
            'before' : bkp
            ,'after' : get_program_holidays( device , [1,2,3,4,5,6,7,8] )
        }
        for k in RISPOSTA['program']['HOLIDAYS']['before'] :
            if k != 'active' :
                prima = RISPOSTA['program']['HOLIDAYS']['before'][k]
                if k in RISPOSTA['program']['HOLIDAYS']['after'] :
                    dopo = RISPOSTA['program']['HOLIDAYS']['after'][k]

                    prima['MOD'] = []
                    for p in ('from','to','temp') :
                        if dopo[p] !=  prima[p]:
                            prima['MOD'].append(p)
                    if len(prima['MOD']) == 1:
                        prima['MOD'] = prima['MOD'][0]
                    elif len(prima['MOD']) == 0 :
                        prima.pop('MOD','')
                else :
                    prima['DEL'] = True

                RISPOSTA['program']['HOLIDAYS']['before'][k] = prima



        for k in RISPOSTA['program']['HOLIDAYS']['after'] :
            if k != 'active' :
                dopo = RISPOSTA['program']['HOLIDAYS']['after'][k]
                if k in RISPOSTA['program']['HOLIDAYS']['before'] :
                    prima = RISPOSTA['program']['HOLIDAYS']['before'][k]
                    dopo['MOD'] = []
                    for p in ('from','to','temp') :
                        if prima[p] !=  dopo[p]:
                            dopo['MOD'].append(p)
                    if len(dopo['MOD']) == 1:
                        dopo['MOD'] = dopo['MOD'][0]
                    elif len(dopo['MOD']) == 0 :
                        dopo.pop('MOD','')
                else :
                    dopo['ADD'] = True

                RISPOSTA['program']['HOLIDAYS']['after'][k] = dopo


    # se avevo mandato in scrittura su program_settings
    # allora adesso vado a vedere se ci sono degli effetti finali
    if pinIsOK() and ('preparing' in RISPOSTA['program']['SETTINGS'] ):
        scriviBT( device , "47e9ee2a-47e9-11e4-8939-164230d1df67", TMP_asByte['PROGRAM_SETTINGS'],'lock e/o manual e/o onoff')
        #tmp_new_settings = program_settings_to_dict(get_program_settings(device))
        #tmp_new_settings.pop('asByte',[])
        #RISPOSTA['program']['SETTINGS']['after'] =  tmp_new_settings
        RISPOSTA['program']['SETTINGS']['after'] = program_settings_to_dict( get_program_settings(device) ) 

        if ((RISPOSTA['program']['SETTINGS']['after']['valvola'] == 'open') and
        (RISPOSTA['program']['SETTINGS']['preparing']['valvola'] == 'open to close') ) :
            RISPOSTA['program']['SETTINGS']['after']['valvola'] = 'open to close'

        if ((RISPOSTA['program']['SETTINGS']['after']['valvola'] == 'close') and
        (RISPOSTA['program']['SETTINGS']['preparing']['valvola'] == 'closed to open') ) :
            RISPOSTA['program']['SETTINGS']['after']['valvola'] = 'closed to open'



    ################################################################
    #                                                              #
    ################################################################
    disconnect("FINE")
    #######################
    if RISPOSTA['gateway']['description'] == None :
        RISPOSTA['gateway'].pop('description',None)

    if toMqtt :

        if topic == '' : 
            topic = topic_root + "/" + mac.upper()
        else : 
            topic = topic_root + "/" + topic

#        if 'description' in RISPOSTA['gateway'] : print(RISPOSTA['gateway']['description'])

        try :
            client = mqtt.Client()
            client.connect(broker, port)
            client.publish(topic=topic, payload=json.dumps(RISPOSTA) ,  qos = 1, retain = True)
        except :
            addLog("main() ERROR" , "Fallito client.connect( %1s , %1s )" % (broker,port), "if toMqtt")


    if RISPOSTA_format == 'json':
        try :
            print(json.dumps(RISPOSTA, sort_keys=False, indent=4))
        except :
            print(RISPOSTA)
    else:
        print(RISPOSTA)
       

if __name__ == "__main__":
    main(sys.argv[1:])


